<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Evologic - Grow. Different.</title>
  <link rel="icon" href="img/favicon.jpg" type="image/gif" sizes="16x16">
  <link rel="stylesheet" type="text/css" href="css/reset.css">
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <link rel="stylesheet" type="text/css" href="css/slider.css">
  <script src="js/jssor.slider-27.1.0.min.js" type="text/javascript"></script>
  <script src="js/slider.js" type="text/javascript"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="js/scroll.js" type="text/javascript"></script>
  <!-- <script src="js/ss.js" type="text/javascript"></script> -->
  <script src="js/kk.js"type="text/javascript"></script>
  <script src="js/board.js"type="text/javascript"></script>
  <!-- <script src="js/show.js"type="text/javascript"></script> -->
  <script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.css"/>
  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick-theme.css"/>
  <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300italic,regular,italic,700,700italic&subset=latin-ext,greek-ext,cyrillic-ext,greek,vietnamese,latin,cyrillic" rel="stylesheet" type="text/css" />
  <link href='https://fonts.googleapis.com/css?family=Playfair+Display:700,900|Fira+Sans:400,400italic' rel='stylesheet' type='text/css'>
  <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
	<!-- <link rel="stylesheet" href="css/timeline.css"> -->
  <link href="https://fonts.googleapis.com/css?family=Cabin:400,700" rel="stylesheet">
	<script src="js/modernizr.js"></script>
  <script src="js/js/ss.js" type="text/javascript"></script>
  <script src="js/js/ss2.js" type="text/javascript"></script>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
  <script src="js/jquery.timelinr-0.9.7.js"></script>
  <script>
    $(function(){
      $().timelinr({
        autoPlay: 'true',
        autoPlayDirection: 'forward'
      })
    });
  </script>
</head>
<body>
  <a href="javascript:" id="return-to-top"><i class="icon-chevron-up"></i></a>
  <div class="menu-wrapper-home">
   <div class="menu">
      <a href="http://www.evologic-technologies.com/" id="logo-link"><img src="img/logo1.png" alt="Evologic Techonologies" /></a>
      <div class="menu-entries top" id="menu-entries" >
          <!-- <div id="menu-entry-1" class="tooltip hov"> -->
              <a href="technology.php" class="main-nav">Technology</a>
          <!-- </div> -->
          <!-- <div  id="menu-entry-2" class="tooltip2 hov"> -->
              <a href="products.php" class="main-nav">Product</a>
          <!-- </div> -->
          <!-- <div  id="menu-entry-3" class="tooltip3 hov"> -->
              <a href="service.php" class="main-nav">Service</a>
          <!-- </div> -->
          <!-- <div  id="menu-entry-3" class="tooltip3 hov"> -->
              <a href="about.php" class="main-nav selected-nav">About Us</a>
          <!-- </div> -->
          <!-- <div id="menu-entry-3" class="tooltip3 hov"> -->
              <a href="investor.php" class="main-nav">Investor Relations</a>
          <!-- </div> -->
      </div>
      <div class="clear"></div>
    </div>

    <section class="top-nav">
      <div>
        <a href="http://www.evologic-technologies.com/" id="logo-link"><img src="img/logo1.png" alt="Evologic Techonologies" /></a>
      </div>

      <input id="menu-toggle" type="checkbox" />
      <label class='menu-button-container' for="menu-toggle">
      <div class='menu-button'></div>
    </label>
      <ul class="menu-mobile">
        <li><a href="technology.php" class="mobile-nav">Technology</a></li>
        <li><a href="products.php" class="mobile-nav">Product</a></li>
        <li><a href="service.php" class="mobile-nav">Service</a></li>
        <li><a href="about.php" class="mobile-nav">About Us</a></li>
        <li><a href="investor.php" class="mobile-nav">Investor Relations</a></li>
      </ul>
  </section>
  </div>

  <div class="header-separator" id="header-separator-about"></div>

  <div data-p="225.00">
      <img data-u="image" src="img/about-us-banner1.jpg" class="banner-img-about" />
  </div>
  <div class="sidenav">
    <div class="technology-img-top">
      <img src="img/icons/about-us1.png" alt="" class="technology-img-left">

    </div>
    <ul>
      <!-- <li class="actives">
        <a href="#phil" class="scroll" >Philosophy</a>
      </li> -->

      <!-- acc change to actives -->
      <li class="actives ">
        <a href="#11" class="scroll" >Team</a>
      </li>
      <li class="acc ">
        <a href="#22" class="scroll" >Scientific Board</a>
      </li>
      <li class="acc ">
        <a href="#33" class="scroll" >Corporate Board</a>
      </li>
    </ul>

  </div>
  <div class="content-about">


    <!-- <div class="short-intro-about"  id="phil">
      <h2 class="short-intro-h2-about-fail">Fail fast, fail forward. The time for perfection is later.</h2>
      <p class="short-intro-about" id="fail-text">We believe that ground-breaking innovations of the 21st century need to feature an increase in sustainability. For the sake of the environment, Evologic is trying to employ biotechnological/ecological approaches. By connecting and managing specialized academic researchers in teams of experts independent of geographic location Evologic functions as scaffold assuring problem focused project team-work.</p>
    </div> -->

  <div class="short-intro-about-scientific-about">
    <h2 class="short-intro-h2-phytotechnicals" id="">What we believe</h2>
    <p class="short-intro-p">We believe that ground-breaking innovations of the 21st century need to feature an increase in sustainability. For most of the challenges of the 21st century, nature actually has a solution. Only our highly standardized production processes have a hard time coping with natural variance. But the source of this variance is commonly a highly dynamic environment. A bioreactor allows to standardize this environment and facilitates the precise reproduction of environmental conditions. For the sake of the environment, Evologic is employing biotechnological and bioengineering approaches to render natural solutions as biostimulants, food or feed additives etc. compatible with the mass market.</p>
  </div>
  <div class="short-intro-about-scientific-about-2" id="11">
    <h2 class="short-intro-h2-phytotechnicals" id="">Operative Team</h2>
    <p class="short-intro-p">Within our fast growing team we pinpoint and follow up on most promising approaches for any given challenge, via the effective use of an online project management tool.</p>
  </div>
  <div class="team1">
    <div class="c1">
    <img class="c1-img-1" src="img/team/WR_smile_02.jpg" alt="">
      <p class="h4-team">Wieland Reichelt</p>
      <p class="p-team">CEO</p>
      <p class="p-team-2">Dr. Wieland Reichelt, coordinates the business development and research efforts and is the main operational interface to the advisory board. In an iterative process the overall business and R&D strategy is continuously challenged by our business development and scientific advisory board, adapted and sharpened to keep the venture within budget and on schedule. His main technical expertise lies within translating academic approaches into industrial solutions for pharmaceutical bioprocesses. Since the founding of Evologic Technologies he has overseen financing efforts, conceptualized the R&D strategies and successfully translated them into IP as well as into several highly competitive grants.</p>
    </div>
    <div class="c2">
      <img class="c1-img-2" src="img/team/MarkusBrillmann_01.jpg" alt="">
      <p class="h4-team">Markus Brillmann</p>
      <p class="p-team">CTO</p>
      <p class="p-team-2">Markus Brillmann, CTO of Evologic is an expert in biotechnological process control. He is leading the operational R&D activities. His expertise lies within complex mechanistic control approaches allowing controlled dynamics in process conditions to maximize the information and process understanding gained per fermentation. This allows fast and efficient exploration of the physiological feasible space of a respective production organism. As co-founder he coordinates the R&D efforts in the area of process development, process modelling (CFD), process engineering and oversees the knowledge management within the team. Supported by our scientific advisory board Markus keeps the R&D projects on track.</p>
    </div>
    <div class="c3">
      <img class="c1-img-3" src="img/team/Gerald_Bacher_01.jpg" alt="">
      <p class="h4-team">Gerold Bacher</p>
      <p class="p-team">Quality</p>
      <p class="p-team-2">Based on his 20 years of expertise within pharmaceutical biotechnology Dr. Bachers main expertise lies within Quality Control and Quality Management. During his career with Böhringer Ingelheim he has overseen the Quality Management for 11 products during pre-clinical, clinical and market phases. In the business unit of process development Dr. Bacher had the project lead for microbial production processes including the analytical method portfolio, resource planning and the evaluation of CMO projects. Dr. Bacher responsibilities cover the development, assessment and establishment of analytical techniques as well as the implementation of state of the art sampling schemes to achieve and maintain high product quality standards.</p>
    </div>
  </div>

  <div class="team2">
    <div class="c4">
      <img class="c1-img-4" src="img/team/Paul_01.jpg" alt="">
      <p class="h4-team">Paul Ruschitzka</p>
      <p class="p-team">Process development and Scale-Up</p>
      <p class="p-team-2">Previously employed for a pharmaceutical biotechnology company, Mr. Ruschitzka´s experience lies within fermentation and primary recovery under GMP conditions. He has overseen Tech-Transfer projects to and from contract manufacturers and was involved in Scale-up activities. Also with his former employer he has been is responsible for process scale up as well as for the implementation and maintenance of GMP guidelines (procurement and qualifying of equipment). His responsibilities cover the coordination of process development as well as the integration of novel findings into our in-silico model to consequently adapt and re-define the correlated up-scaling efforts.</p>
    </div>
    <div class="c5">
      <img class="c1-img-5" src="img/team/Maximilian_Schell_01.jpg" alt="">
      <p class="h4-team">Maximilian Schmid</p>
      <p class="p-team">Reactor design</p>
      <p class="p-team-2">The main expertise of Mr. Schmid lies within low cost bioreactor design for high throughput screenings. During his time in academia he has investigated anaerobe microbials on complex media sources. He has consequently integrated the gained knowledge into the ongoing bioprocess development to finally yield unprecedented increases of turnover rates. Within the team he is operating bioreactor setups and responsible the integration of real time analytics into the PIMS as well as for process data evaluation.</p>
    </div>
    <div class="c6">
      <img class="c1-img-6" src="img/team/Conrad.jpg" alt="">
      <p class="h4-team">Conrad Reichelt</p>
      <p class="p-team">Public Relations</p>
      <p class="p-team-2">Personally affine to design aspects, Mr. Reichelt is responsible for Evologic´s public appearances, cooperate identify and cooperate design. His efforts span the establishment and maintenance of design guidelines as well as the composition and realization of transportable experimental setups for public appearances of Egologic Technologies. In addition Mr. Reichelt is handling the communication to public sources/news feeds etc.</p>
    </div>
  </div>

  <div class="team3">
    <div class="c7">
      <img class="c1-img-7" src="img/team/Verena.jpg" alt="">
      <p class="h4-team">Verena Grünwald</p>
      <p class="p-team">Administration</p>
      <p class="p-team-2">Internationally experienced in accounting, controlling and marketing with various companies. Mrs. Grünwald is responsible for accounting, controlling and billing as well as for the day to day administration of Evologic Technologies. She constitutes the interface to our tax attorneys as well as to public institutions and ensures prompt and accurate controlling and accounting reports.</p>
    </div>
    <div class="c8">
      <img class="c1-img-7" src="img/team/saleh.jpg" alt="">
      <p class="h4-team">Saleh Akrami</p>
      <p class="p-team">IT</p>
      <p class="p-team-2">As a computer scientist has worked with various projects with USAID, ISAF and NATO where he had been assigned ICT and network responsibilities. At Evologic Technologies his responsibilities cover building up and maintaining the IT infrastructure. This includes the regular maintenance, but more importantly regular improvements of our workstations to comply with the increasing computing demands of our neuronal networks and computational flow dynamics. Moreover, Mr. Akrami is responsible for data and network security at Evologic Technologies.</p>
    </div>

  </div>

  <div class="short-intro-about-scientific-about-2" id="22">
    <h2 class="short-intro-h2-phytotechnicals" id="">Scientific Advisory board</h2>
    <p class="short-intro-p">Ongoing research projects are constantly discussed and critically reviewed with a broad range of experts in the field. This approach allows using external know how to clarify and focus development strategies. To keep the discussions lean and output oriented the whole scientific expert board is only consulted twice a year, while specific questions are discussed bilateral at a high frequency. Selection criteria are know how in the field and/or in the relevant industries but more importantly a personal tendency for practical problem solutions.</p>
  </div>

  <div class="team4">
    <div class="c9">
      <img class="c1-img-7" src="img/christoph-herwig.jpg" alt="">
      <p class="h4-team">Prof. Christoph Herwig</p>
      <p class="p-team">Process Engineering/Process Control</p>
      <p class="p-team-2">As a full Prof. at the TU Vienna, Prof. Herwig (> 200 publications) core competencies cover the quantification and de-consequent bottlenecking of bioprocess performance based on advanced data exploitation approaches and mechanistic modelling. Using novel technologies for online and real time monitoring he is pushing biochemical engineering to be regarded on par to genetic engineering. Experienced in biopharmaceutical facility design (GMP conditions) Prof. Herwig is helping to leverage process productivity.</p>
    </div>
    <div class="c10">
      <img class="c1-img-7" src="img/placeholder.jpg" alt="">
      <p class="h4-team">Dr. Stuart Stocks</p>
      <p class="p-team">Reactor Design/Upscale</p>
      <p class="p-team-2">Briding the gap between academia and industry Dr. Stocks has gathered extensive know how in upscaling processes in the field of white biotech from 1L to >100 m3 as wells as in red biotech. His legacy is the successful development of the production process for cellulases used for 2. Gen ethanol. Dr. Stocks is specifically experienced in the field of aeration, agitation and impeller design as well as in process development, making him the key source of Know How regarding reactor design and upscaling challenges.</p>
    </div>
    <div class="c11">
      <img class="c1-img-7" src="img/boardimg/M_Giovanetti.PNG" alt="">
      <p class="h4-team">Dr. Marco Giovanetti</p>
      <p class="p-team">AMF Biology</p>
      <p class="p-team-2">His comprehensive background in plant biotechnology and his research on the molecular mechanisms of AMF symbiosis make him one of the view existing  experts on plant/AMF interaction and AMF biochemistry. His input is especially valuable for the advancements of AMF analytics and general product quality aspects.</p>
    </div>

  </div>

  <div class="team5">
    <div class="c12">
      <img class="c1-img-7" src="img/boardimg/D_Eibl.jpg" alt="">
      <p class="h4-team">Prof. Eibl</p>
      <p class="p-team">HR Biology</p>
      <p class="p-team-2">Prof. Eibl (~100 publications) is one of a view veritable experts in hairy root cultivation techniques, who has been actively involved in the development of related production technologies for industrial applications. As head of the Competence Center for Biochemical Engineering and Cell Cultivation Technique of the ICBT and head of the working group Biochemical Engineering at the ZHAW he is supporting Evologic with his extensive Know How in hairy root handling and processing.</p>
    </div>
    <div class="c13">
      <img class="c1-img-7" src="img/nor.JPG" alt="">
      <p class="h4-team">Dr. Norbert Reichelt</p>
      <p class="p-team">IP strategy</p>
      <p class="p-team-2">As author of over 50 scientific publications and inventor of over 32 patents with his current employer, Dr. Reichelt is well experienced in the field of IP protection in a highly competitive industrial environment. In the more recent past Dr. Reichelt has been occupied with technology scouting and identification and consequent management of strategic technology projects. Today he is responsible for New Business Concepts, Technology evaluation for large scale investments >600 Mio. In this board Dr. Reichelt constitutes the key source of expertise for strategic decisions regarding IP protection and licensing.</p>
    </div>
    <div class="c14">
      <img class="c1-img-7" src="img/boardimg/S_Muellner.jpg" alt="">
      <p class="h4-team">Dr. Stefan Müllner</p>
      <p class="p-team">Product to Market Fit</p>
      <p class="p-team-2">As biochemist by training, Dr. Müllner is expert for industrial biotechnology and experienced entrepreneur (Protagen). In his >30 y industrial carrier, at Hoechst, Aventis, Henkel, Protagen he has lead various products from bench to market and authored >100 publications.</p>
    </div>
  </div>

    <div class="short-intro-about-scientific-about-2" id="33">
      <h2 class="short-intro-h2-phytotechnicals" id="">Corporate Advisory Board</h2>
      <p class="short-intro-p">Overal strategic decisions concerning market entry and business development are discussed and pitched to the sounding board which is composed of highly experienced individuals. This allows reviewing out-of-the-box products and problem solutions given the independence of the board.</p>
    </div>
    <div class="team6">
      <div class="c15">
        <img class="c1-img-7" src="img/hans-bodingbauer.jpg" alt="">
        <p class="h4-team">Dr. Hans bodingbauer</p>
        <!-- <p class="p-team">Administration</p> -->
        <p class="p-team-2">With his strong international industrial (Agro Linz, DuPont, BayWa) background in  product development (agrochemicals) and controlling, Dr. Bodingbauer was a successful entrepreneur (Netragon AG) and is leading the business development sounding board of Evologic Technologies.</p>
      </div>
      <div class="c16">
        <img class="c1-img-7" src="img/business/P_Innes.PNG" alt="">
        <p class="h4-team">Dr. Peter innes</p>
        <p class="p-team">Business development</p>
        <p class="p-team-2">During his time as Chief Executive Officer, Becker Underwood was acquired by BASF in 2012. Prior to this exit, he successfully expanded the company expanded from a single facility business in Iowa to a global company with operations on five continents. Prior to joining Becker Underwood, Peter was Group Managing Director of The MicroBio Group Limited, a manufacturer of bio-fertilizers and crop protection products that was acquired by Becker Underwood in 2000. </p>
      </div>

      <div class="c16">
        <img class="c1-img-7" src="img/business/R_Witte.PNG" alt="">
        <p class="h4-team">Dr. Rolf Witte</p>
        <p class="p-team-2">As experienced entrepreneur and CEO/CFO of various companies (Neuraxo Pharmaceuticals, Contract Medical International, PSites Pharma, Bert Energy GmbH) his key expertise lies within financing and term sheet negotiations. Dr. Witte has background as a lawyer and auditor and is and successfully leading investment rounds.</p>
      </div>

    </div>
  </div>
  <!-- <div class="content-team" id="team">
    <h3 class="agb-h31">Interdisciplinary team</h3><br>
    <div class="team-div-ceo">
      <p class="team-ceo">Dr. Wieland Reichelt</p>
      <p class="team-text-left-ceo">CEO, Dr. Wieland Reichelt, coordinates this project and is the main operational interface to the advisory board. In an iterative process the overall business and R&D strategy is continoulsy challenged, adapted an sharpened to keep the venture within budget and on schedule. His main technical expertise lies within translating academic approaches into industrial solutions for pharmaceutical bioprocesses. </p>
      <img class="team-img-ceo" src="img/wieland.jpg" alt="Wieland Reichelt">
    </div>

    <div class="team-div-res">
      <img class="team-img-res" src="img/Markus_HP_.jpg" alt="Markus Brillmann">
      <p class="team-res">Markus Brillman</p>
      <p class="team-text-left">Markus Brillman, CTO and expert in biotechnological process control is operationally leading the R&D activities. They are co-founders of Evologic and have together with their team already successfully introduced ‘gaia soil’ into the B2C market. ‘Gaia soil’ is soil enriched with AMF to increase the effect of fertilizers. </p>
    </div>

    <div class="team-div-fin">
      <p class="team-fin">Dr Norbert Reichelt</p>
      <p class="team-text-left" id="fin">CFO, Dr Norbert Reichelt (New Business Concepts, Technology evaluation for large scale investments >600Mio, Borealis GMbh) is the third co-founder. His main tasks is mentoring the management team, including our CFO and head of business. </p>
      <img class="team-img-fin" src="img/nor.JPG" alt="Norbert Reichelt">
    </div>


    <div class="team-div-des">
      <img class="team-img-des" src="img/Conrad_HP_.jpg" alt="Conrad Reichelt">
      <p class="team-des">Conrad Reichelt</p>
      <p class="team-text-left" id="des">Marketting, Conrad Reichelt is coordinating marketing efforts for the B2C Product as well as the public appearance of evologic technologies. </p>
    </div>

    <div class="short-intro-about-scientific" id="board">
      <h2 class="short-intro-h2">Scientific Experts/Advisory Board</h2>
      <p class="short-intro-p">Ongoing research projects are constantly discussed and critically reviewed with a broad range of experts in the field. This approach allows using external know how to clarify and focus development strategies. To keep the discussions lean and output oriented the whole scientific expert board is only consulted twice a year, while specific questions are discussed bilateral at a high frequency. Selection criteria are know how in the field and/or in the relevant industries but more importantly a personal tendency for practical problem solutions. </p>
    </div>


  </div>


  <div class="hid">

  </div>
    <div class="logoss-container" id="logoss-container">
       <section class="customer-logos slider">
          <div class="slide">
            <div class="team-div-ceo-1">
        <p class="team-ceo-1" id="proc">Prof. Christoph Herwig</p>
        <p class="team-text-left-ceo-1">Prof. Christoph Herwig, Process expert in biopharmaceutical facility design. Vast experience in using bioprocess technology to leverage productivity. </p>
        <img class="team-img-other" id="christoph-img" src="img/christoph-herwig.jpg" >
      </div>
          </div>

          <div class="slide">

                  <div class="team-div-ceo-1">
              <p class="team-ceo-1" id="react">Dr. Stuart Stocks</p>
              <p class="team-text-left-ceo-1">Dr. Stuart Stocks, Briding the gap between academia and industry Dr. Stocks has gathered extensive know how in upscaling processes in the field of white biotech from 1L to >100 m3 as wells as in red biotech. His legacy is the sucessful development of the production process for cellulases used for 2. Gen ethanol  </p>
              <img class="team-img-other1" id="dr-stuart-stocks" src="img/placeholder.jpg" >
            </div>

          </div>

          <div class="slide">

                  <div class="team-div-ceo-1">
              <p class="team-ceo-1" id="amf-biology">Dr. Marco Giovanetti</p>
              <p class="team-text-left-ceo-1">Dr. Marco Giovanetti, His comprehensive background in plant biotechnology and his research on the molecular mechanisms of AMF symbiosis make him an expert on plant/AMF interaction. </p>
              <img class="team-img-other2" id="dr-marco-giovanetti-img" src="img/placeholder.jpg" >
            </div>

          </div>

           <div class="slide">

                  <div class="team-div-ceo-1">
              <p class="team-ceo-1" id="ip-strategy">Dr. Norbert Reichelt</p>
              <p class="team-text-left-ceo-1" >Norbert Reichelt, Vastly experience in technology/IP screening and evaluation especially as technology responsible in merger and aquisitions for projects >500mio Volume. Development and implementation expert of new business concepts</p>
              <img class="team-img-other3" id="dr-norbert-reichelt-img" src="img/nor.JPG" >
            </div>

          </div>

          <div class="slide">

                  <div class="team-div-ceo-1">
              <p class="team-ceo-1" id="products-market">Dr. Stefan Müllner</p>
              <p class="team-text-left-ceo-1">Dr. Stefan Müllner, As Scientist, successful founder and as executive of various companies (e.g. Protagen) Dr. Müllner has lead various products from bench to market. </p>
              <img class="team-img-other4" id="dr-stefan-mullner-img" src="img/placeholder.jpg" >
            </div>

          </div>

       </section>
    </div>



    <div class="short-intro-about-scientific-sounding-board" id="soundingboard">
      <h2 class="short-intro-h2">Sounding Board</h2>
      <p class="short-intro-p-sounding-board">Overal strategic decisions concerning market entry decisions and business development are discussed and pitched to the sounding board which is composed from highly experienced individuals. This allows reviewing out-of-the-box products and problem solutions since </p>
    </div>

    <div class="hid">

    </div>
      <div class="logoss-container" id="logoss-container">
         <section class="customer-logos slider">
            <div class="slide">
              <div class="team-div-ceo-1">
          <p class="team-ceo-1" id="proc">Hans Bodingbauer</p>
          <p class="team-text-left-ceo-1">With his strong international industrial (Agro Linz, DuPont, BayWa) background in  product development (agrochemicals) and controling, Dr. Bodingbauer was a successfull enterpreneur (Netragon AG) and is leading the business develompent sounding board of Evologic Technologies.</p>
          <img class="team-img-other5" id="hans-img" src="img/hans-bodingbauer.jpg" >
        </div>
            </div>
            <div class="slide">

                    <div class="team-div-ceo-1">
                <p class="team-ceo-1" id="amf-biology">Rolf Witte</p>
                <p class="team-text-left-ceo-1">As experienced entrepreneur and CEO/CFO of various companies (Neuraxo Pharmaceuticals, Contract Medical International, PSites Pharma, Bert Energy GmbH) his key expertise lies within financing and successfully leading investment rounds.</p>
                <img class="team-img-other7" id="dr-marco-img" src="img/rolf-witte.jpg" >
              </div>

            </div>
            <div class="slide">

                    <div class="team-div-ceo-1">
                <p class="team-ceo-1" id="react">Peter Innes</p>
                <p class="team-text-left-ceo-1">During his time as Chief Executive Officer, Becker Underwood was acquired by BASF in 2012. Prior to this exit, he successfully expanded the company expanded from a single facility business in Iowa to a global company with operations on five continents. Prior to joining Becker Underwood, Peter was Group Managing Director of The MicroBio Group Limited, a manufacturer of bio-fertilizers and crop protection products that was acquired by Becker Underwood in 2000. </p>
                <img class="team-img-other6" id="dr-stuart-stocks" src="img/placeholder.jpg" >
              </div>

            </div>

            <div class="slide">

                    <div class="team-div-ceo-1">
                <p class="team-ceo-1" id="amf-biology">Rolf Witte</p>
                <p class="team-text-left-ceo-1">As experienced entrepreneur and CEO/CFO of various companies (Neuraxo Pharmaceuticals, Contract Medical International, PSites Pharma, Bert Energy GmbH) his key expertise lies within financing and successfully leading investment rounds.</p>
                <img class="team-img-other7" id="dr-marco-img" src="img/rolf-witte.jpg" >
              </div>

            </div>

         </section>
      </div> -->
      <!-- <div id="mymessage">
        Click!
      </div> -->
      <!-- partner logo2 slider end -->
      <h2 class="short-intro-h2" id="timeline1">Timeline</h2>
      <div id="timeline">
          <ul id="dates">
            <li><a href="#1900">17 Feb</a></li>
            <li><a href="#1930">01 Mar</a></li>
            <li><a href="#1944">02 Apr</a></li>
            <li><a href="#1950">12 Aug</a></li>
            <li><a href="#1971">15 Oct</a></li>
            <li><a href="#1977">12 May</a></li>
            <li><a href="#1989">24 Jun</a></li>
            <li><a href="#1999">27 Oct</a></li>
            <li><a href="#2001">09 Nov</a></li>
            <li><a href="#2011">16 Nov</a></li>
          </ul>
          <ul id="issues">
            <li id="1900">
              <img src="img/tuu.jpeg" width="400" height="256" />
              <h1>17 Feb 2016</h1>
              <p>AWARD WINNER OF THE I2C START UP ACADEMY.</p>
            </li>
            <li id="1930">
              <img src="img/march.jpg" width="400" height="256" />
              <h1>01 March 2016</h1>
              <p>FUNDING FROM INITS RECEIVED.</p>
            </li>
            <li id="1944">
              <img src="img/evv.png" width="400" height="256" />
              <h1>02 Apr 2016</h1>
              <p>ESTABLISHED A GMBH.</p>
            </li>
            <li id="1950">
              <img src="img/12.jpeg" width="400" height="256" />
              <h1>12 Aug 2016</h1>
              <p>PICKED TO ENTER RWA AGRO INNOVATION LAB.</p>
            </li>
            <li id="1971">
              <img src="img/ag.jpg"  height="256" />
              <h1>15 Oct 2016</h1>
              <p>MOST PROMISING START UP OF THE AGRO INNOVATION LAB AT RWA</p>
            </li>
            <li id="1977">
              <img src="img/may.jpg" width="400" height="256" />
              <h1>12 May 2017</h1>
              <p>WINNER OF SCIENCE 2 BUSINESS AWARD</p>
            </li>
              <li id="1989">
              <img src="img/aaa.png" width="400" height="256" />
              <h1>24 Jun 2017</h1>
              <p>RWA AND BAYWA INVEST IN EVOLOGIC TECHNOLOGIES GMBH</p>
            </li>
            <li id="1999">
              <img src="img/ak.jpg" width="400" height="256" />
              <h1>27 Oct 2017</h1>
              <p>MERCUR AWARD VIDEO DREH</p>
            </li>
            <li id="2001">
              <img src="img/09.png" width="400" height="256" />
              <h1>09 Nov 2017</h1>
              <p>LARGE SCALE MYKORRHIZA FIELD TRIAL FINISHED WITH PROMISING RESULTS</p>
            </li>
            <li id="2011">
              <img src="img/aw.jpg" width="400" height="256" />
              <h1>16 Nov 2017</h1>
              <p>WINNER OF MERCUR AWARD FOR GREEN ECONOMY</p>
            </li>
          </ul>
          <div id="grad_left"></div>
          <div id="grad_right"></div>
          <a href="#" id="next">+</a>
          <a href="#" id="prev">-</a>
        </div>



<div class="footer">
    <div class="footer-separator"></div>
    <div class="footer-left">
      <p class="contact">Contact:</p>
      <p class="buro">office +43 324324856 </p>
      <a class="office" href="mailto:office@evologic.at?subject=feedback" target="_blank">office@evologic.at</a><br>
      <a class="www" href="http://www.evologic.at" target="_blank"> www.evologic.at</a>
    </div>
    <div class="footer-right">
      <ul class="ul-footer">
        <li class="home"><a class="home" href="http://www.evologic.at" target="_blank">Home</a></li>
        <li><a class="agb" href="agb.php" target="_blank">AGB</a></li>
        <li class="impli"><a class="imp" href="impressum.php" target="_blank">Impressum</a></li>
      </ul>
    </div>
</div>

  <script>

      var elements = document.getElementsByClassName("child");
      for(var i = 0; i < elements.length; i++)
      {
          elements[i].onclick = function(){

              // remove class from sibling

              var el = elements[0];
              while(el)
              {
                  if(el.tagName === "DIV"){
                      //remove class
                      el.classList.remove("bak");

                  }
                  // pass to the new sibling
                  el = el.nextSibling;
              }

            this.classList.add("bak");
          };
      }

  </script>
<script type="text/javascript">jssor_1_slider_init();</script>
<script type="text/javascript" src="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.min.js"></script>

<!-- <script src="js/jquery-2.1.4.js"></script>
<script src="js/jquery.mobile.custom.min.js"></script> -->
<script src="js/main.js"></script>
<script src="js/smooth.js"type="text/javascript"></script>
<script src="js/top.js" type="text/javascript"></script>

</body>
</html>

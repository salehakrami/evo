<?php
session_start();
$loggedIn = false;
if (isset($_SESSION['uid'])) {
    $loggedIn = true;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Evologic - Grow. Different.</title>
  <link rel="icon" href="img/favicon.jpg" type="image/gif" sizes="16x16">
  <link rel="stylesheet" type="text/css" href="css/reset.css">
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <link rel="stylesheet" type="text/css" href="css/slider.css">
  <script src="js/jssor.slider-27.1.0.min.js" type="text/javascript"></script>
  <script src="js/slider.js" type="text/javascript"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="js/scroll.js" type="text/javascript"></script>
  <script src="js/ss.js" type="text/javascript"></script>
  <script src="js/kk.js"type="text/javascript"></script>
  <script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.css"/>
  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick-theme.css"/>
  <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300italic,regular,italic,700,700italic&subset=latin-ext,greek-ext,cyrillic-ext,greek,vietnamese,latin,cyrillic" rel="stylesheet" type="text/css" />
  <link href='https://fonts.googleapis.com/css?family=Playfair+Display:700,900|Fira+Sans:400,400italic' rel='stylesheet' type='text/css'>
  <link href="https://fonts.googleapis.com/css?family=Cabin:400,700" rel="stylesheet">
  <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
	<link rel="stylesheet" href="css/timeline.css"> <!-- Resource style -->
	<script src="js/modernizr.js"></script> <!-- Modernizr -->
  <script src="js/jssor.slider-27.1.0.min.js" type="text/javascript"></script>
  <script src="js/slider.js" type="text/javascript"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <!-- <script src="js/scroll.js" type="text/javascript"></script>
  <script src="js/ss.js" type="text/javascript"></script>
  <script src="js/kk.js"type="text/javascript"></script> -->
  <script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
  <script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.css"/>
  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick-theme.css"/>
  <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300italic,regular,italic,700,700italic&subset=latin-ext,greek-ext,cyrillic-ext,greek,vietnamese,latin,cyrillic" rel="stylesheet" type="text/css" />
  <script src='https://www.google.com/recaptcha/api.js'></script>
  <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Cabin:400,700" rel="stylesheet">


</head>
<body>
  <a href="javascript:" id="return-to-top"><i class="icon-chevron-up"></i></a>
  <div class="menu-wrapper-home">
   <div class="menu">
      <a href="http://www.evologic-technologies.com/" id="logo-link"><img src="img/logo1.png" alt="Evologic Techonologies" /></a>
      <div class="menu-entries top" id="menu-entries" >
          <!-- <div id="menu-entry-1" class="tooltip hov"> -->
              <a href="technology.php" class="main-nav">Technology</a>
          <!-- </div> -->
          <!-- <div  id="menu-entry-2" class="tooltip2 hov"> -->
              <a href="products.php" class="main-nav">Product</a>
          <!-- </div> -->
          <!-- <div  id="menu-entry-3" class="tooltip3 hov"> -->
              <a href="service.php" class="main-nav">Service</a>
          <!-- </div> -->
          <!-- <div  id="menu-entry-3" class="tooltip3 hov"> -->
              <a href="about.php" class="main-nav">About Us</a>
          <!-- </div> -->
          <!-- <div id="menu-entry-3" class="tooltip3 hov"> -->
              <a href="investor.php" class="main-nav selected-nav">Investor Relations</a>
          <!-- </div> -->
      </div>
      <div class="clear"></div>
    </div>

    <section class="top-nav">
      <div>
        <a href="http://www.evologic-technologies.com/" id="logo-link"><img src="img/logo1.png" alt="Evologic Techonologies" /></a>
      </div>

      <input id="menu-toggle" type="checkbox" />
      <label class='menu-button-container' for="menu-toggle">
      <div class='menu-button'></div>
    </label>
      <ul class="menu-mobile">
        <li><a href="technology.php" class="mobile-nav">Technology</a></li>
        <li><a href="products.php" class="mobile-nav">Product</a></li>
        <li><a href="service.php" class="mobile-nav">Service</a></li>
        <li><a href="about.php" class="mobile-nav">About Us</a></li>
        <li><a href="investor.php" class="mobile-nav">Investor Relations</a></li>
      </ul>
  </section>



  </div>
  <div class="header-separator" id="header-separator-products"></div>
  <div id="jssor_1" style="position:relative;top:130px;left:0px;width:1300px;height:360px;overflow:hidden;visibility:hidden;">
      <!-- Loading Screen -->
      <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
          <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="img/spin.svg" />
      </div>
      <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:1300px;height:340px;overflow:hidden;">
          <div data-p="225.00">
              <img data-u="image" src="img/investors-relation-banner1.jpg" class="banner-img" />
          </div>
      </div>
  </div>
  <div class="content-investor">
    <div class="sidenav">
      <img src="img/icons/investor-relations1.png" alt="" class="technology-img-left">

      <ul>
        <li class="actives ">
          <a href="#11" class="scroll" >Investor relations</a>
        </li>
        <li class="acc ">
          <a href="#22" class="scroll" >Current investors</a>
        </li>
        <li class="acc ">
          <a href="#33" class="scroll" >Current need for capital</a>
        </li>
      </ul>
    </div>


    <div id="11" class="short-intro-about">
      <p class="short-intro-p">For advancements in the sector or white/green biotechnology and bioengineering it needs more than a desk a PC and a chair. This differentiates us in regard of capital demand from the common IKT SpinOff but facilitates comprehensive IP protection measures, which constitute the basis for the intrinsic value of our technology development. Our business model once was called a business model of the old economy by a renowned Business Angel. We believe this is correct, but we are motivated by the perspective of establishing the foundation for physical products which have the potential to help us grow into a sustainable future.</p>
    </div>

    <div class="short-intro-about-scientific" id="22">
      <h2 class="short-intro-h2-phytotechnicals" id="">Current Investors</h2>
      <p class="short-intro-p">The current majority shareholders are still the founders who are holding approx. 70% of the shares. Investors and mentors are and have been a Business Angel of the Vivida Beteiligungs GmbH, an early stage VC, Fritz Mauthner GmbH & Co KG, a seed trader and RWA Raiffeisen Ware Austria AG/BayWa, two major seed distributers in the whole of Europe.</p>
    </div>
    <div class="short-intro-about-scientific">
      <img data-u="image" src="img/partner_logos/1.png" class="banner-transition-investor">
      <img data-u="image" src="img/partner_logos/6.png" class="banner-transition-investor">
      <img data-u="image" src="img/partner_logos/5.png" class="banner-transition-investor">
      <img data-u="image" src="img/partner_logos/vivida.jpg" class="banner-transition-investor">


    </div>
    <div class="short-intro-about-scientific investor-last-content" id="33">
      <h2 class="short-intro-h2-phytotechnicals" id="">Current need for capital</h2>
      <p class="short-intro-p">We are currently looking to raise capital from an investor with experience in bioprocess engineering and scale up in order to finance the process scale-up to 15m3 production scale. If our venture fits your scope please contact office@evologic-technologies.com for further information and to initiate discussions.</p>
    </div>



  </div>

    <?php



    require_once'security.php';

    $errors = isset($_SESSION['errors']) ?  $_SESSION['errors'] : [];
    $fields = isset($_SESSION['fields']) ?  $_SESSION['fields'] : [];

    ?>
    <div class="contact-form" style="margin-top:-200px;">

      <h1>Contact Us</h1>
      <div class="container-investor">
        <form id="contact-form" method="post" action="check.php">
          <label for="fname">Full Name</label>
          <input type="text" name="title" class="hfield">
          <input type="text" id="form_fname" name="name" placeholder="Full Name" >
          <span style="color:red;" class="error_form" id="fname_error_message"></span>
          <input type="text" id="form_email" name="email" placeholder="Email Address">
          <span style="color:red;" class="error_form" id="email_error_message"></span>
          <textarea id="form_message" name="message" placeholder="Your Message" style="height:140px" ></textarea>
          <span style="color:red;" class="error_form" id="message_error_message"></span>
          <div style="margin-bottom: 10px; margin-top:5px;"class="g-recaptcha" data-sitekey="6LcfclcUAAAAAGNylZI8zih_qfHfF7yYg7D8F0CQ"></div>
          <input type="submit" onclick="scrollToElement('#contact-form');" value="Submit">
        </form>
        <?php
          unset($_SESSION['errors']);
          unset($_SESSION['fields']);
        ?>
      </div>
      <div class="frame">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d9474142.548590409!2d8.562849457341615!3d48.199281260462456!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x476d0785a5a1eec5%3A0xc67c7257c0a7cc9f!2sGumpendorfer+Str.+19%2C+1060+Wien!5e0!3m2!1sen!2sat!4v1527257347518" width="400" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
      </div>
    </div>

  <div class="footer">
      <div class="footer-separator"></div>
      <div class="footer-left">
        <p class="contact">Contact:</p>
        <p class="buro">office +43 324324856 </p>
        <a class="office" href="mailto:office@evologic.at?subject=feedback" target="_blank">office@evologic.at</a><br>
        <a class="www" href="http://www.evologic.at" target="_blank"> www.evologic.at</a>
      </div>
      <div class="footer-right">
        <ul class="ul-footer">
          <li class="home"><a class="home" href="http://www.evologic.at" target="_blank">Home</a></li>
          <li><a class="agb" href="agb.php" target="_blank">AGB</a></li>
          <li class="impli"><a class="imp" href="impressum.php" target="_blank">Impressum</a></li>
        </ul>
      </div>
  </div>
  <script>
    $(function(){
      $("#fname_error_message").hide();
      $("#email_error_message").hide();
      $("#message_error_message").hide();
      var error_fname = false;
      var error_message = false;
      var error_email = false;

      $("#form_fname").focusout(function(){
        check_fname();
      });
      $("#form_email").focusout(function() {
        check_email();
      });
      $("#form_message").focusout(function() {
        check_message();
      });

      function check_fname() {
      var pattern = /^[a-zA-Z 0-9\n\\-\\s]+$/;
      var fname = $("#form_fname").val();
      if (pattern.test(fname) && fname !== '') {
      $("#fname_error_message").hide();
      $("#form_fname").css("border-bottom","2px solid #34F458");
      } else {
      $("#fname_error_message").html("Please type your name");
      $("#fname_error_message").show();
      $("#form_fname").css("border-bottom","2px solid #F90A0A");
      error_fname = true;
      }
      }

      function check_message() {
      var pattern = /^[a-zA-Z 0-9\n\\-\\s]+$/;
      var message = $("#form_message").val()
      if (pattern.test(message) && message !== '') {
      $("#message_error_message").hide();
      $("#form_message").css("border-bottom","2px solid #34F458");
      } else {
      $("#message_error_message").html("Message should only contain characters,numbers and space");
      $("#message_error_message").show();
      $("#form_message").css("border-bottom","2px solid #F90A0A");
      error_message = true;
      }
      }

      function check_email() {
      var pattern = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
      var email = $("#form_email").val();
      if(pattern.test(email) && email !== '') {
      $("#email_error_message").hide();
      $("#form_email").css("border-bottom","2px solid #34F458");
      } else {
      $("#email_error_message").html("Invalid Email");
      $("#email_error_message").show();
      $("#form_email").css("border-bottom","2px solid #F90A0A");
      error_email = true;
      }
      }

      $("#contact-form").submit(function() {
      error_fname = false;
      error_message = false;
      error_email = false;

      check_fname();
      check_message();
      check_email();
      if (error_fname === true) {
      alert("Please Fill the form Correctly");
      return false;
    }
      if (error_message === true) {
      alert("Please Fill the form Correctly");
      return false;
    }
      if (error_email === true) {
      alert("Please Fill the form Correctly");
      return false;
    }
      if(grecaptcha && grecaptcha.getResponse().length > 0)
        {
          alert("Thank you for your message, we will contact you soon!");
          return true;
        }
        else
        {
            alert('Oops, you have to check the recaptcha !');
            return false;
        }
      });
    });


      var elements = document.getElementsByClassName("child");
      for(var i = 0; i < elements.length; i++)
      {
          elements[i].onclick = function(){

              // remove class from sibling

              var el = elements[0];
              while(el)
              {
                  if(el.tagName === "DIV"){
                      //remove class
                      el.classList.remove("bak");

                  }
                  // pass to the new sibling
                  el = el.nextSibling;
              }

            this.classList.add("bak");
          };
      }

  </script>
<script type="text/javascript">jssor_1_slider_init();</script>
<script type="text/javascript" src="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.min.js"></script>
<script src="js/top.js" type="text/javascript"></script>
  <script>

      var elements = document.getElementsByClassName("child");
      for(var i = 0; i < elements.length; i++)
      {
          elements[i].onclick = function(){

              // remove class from sibling

              var el = elements[0];
              while(el)
              {
                  if(el.tagName === "DIV"){
                      //remove class
                      el.classList.remove("bak");

                  }
                  // pass to the new sibling
                  el = el.nextSibling;
              }

            this.classList.add("bak");
          };
      }

  </script>
<script type="text/javascript">jssor_1_slider_init();</script>
<script type="text/javascript" src="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.min.js"></script>
<script src="js/jquery-2.1.4.js"></script>
<script src="js/jquery.mobile.custom.min.js"></script>
<script src="js/main.js"></script> <!-- Resource jQuery -->
<script src="js/js/smooth.js"type="text/javascript"></script>
<script src="js/top.js" type="text/javascript"></script>
</body>
</html>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Evologic - Grow. Different.</title>
  <link rel="icon" href="img/favicon.jpg" type="image/gif" sizes="16x16">
  <link rel="stylesheet" type="text/css" href="css/reset.css">
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <link rel="stylesheet" type="text/css" href="css/slider.css">
  <script src="js/jssor.slider-27.1.0.min.js" type="text/javascript"></script>
  <script src="js/slider.js" type="text/javascript"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="js/scroll.js" type="text/javascript"></script>
  <script src="js/ss.js" type="text/javascript"></script>
  <script src="js/kk.js"type="text/javascript"></script>

  <script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.css"/>
  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick-theme.css"/>
  <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300italic,regular,italic,700,700italic&subset=latin-ext,greek-ext,cyrillic-ext,greek,vietnamese,latin,cyrillic" rel="stylesheet" type="text/css" />
  <link href='https://fonts.googleapis.com/css?family=Playfair+Display:700,900|Fira+Sans:400,400italic' rel='stylesheet' type='text/css'>
  <link href="https://fonts.googleapis.com/css?family=Cabin:400,700" rel="stylesheet">
  <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
	<link rel="stylesheet" href="css/timeline.css"> <!-- Resource style -->
	<script src="js/modernizr.js"></script> <!-- Modernizr -->
</head>
<body>
  <script type="text/javascript">
    var password;
    var pass="EVT123@";
    password=prompt("Please enter the password to see the content!");
    if(password==pass)
    alert("Access Granted!");
    else {
      window.location="index.php";
    }
  </script>
  <a href="javascript:" id="return-to-top"><i class="icon-chevron-up"></i></a>
  <div class="menu-wrapper-home">
   <div class="menu">
      <a href="http://www.evologic-technologies.com/" id="logo-link"><img src="img/logo1.png" alt="Evologic Techonologies" /></a>
      <div class="menu-entries top" id="menu-entries" >
          <!-- <div id="menu-entry-1" class="tooltip hov"> -->
              <a href="technology.php" class="main-nav selected-nav">Technology</a>
          <!-- </div> -->
          <!-- <div  id="menu-entry-2" class="tooltip2 hov"> -->
              <a href="products.php" class="main-nav">Product</a>
          <!-- </div> -->
          <!-- <div  id="menu-entry-3" class="tooltip3 hov"> -->
              <a href="service.php" class="main-nav">Service</a>
          <!-- </div> -->
          <!-- <div  id="menu-entry-3" class="tooltip3 hov"> -->
              <a href="about.php" class="main-nav">About Us</a>
          <!-- </div> -->
          <!-- <div id="menu-entry-3" class="tooltip3 hov"> -->
              <a href="investor.php" class="main-nav">Investor Relations</a>
          <!-- </div> -->
      </div>
      <div class="clear"></div>
    </div>

    <section class="top-nav">
      <div>
        <a href="http://www.evologic-technologies.com/" id="logo-link"><img src="img/logo1.png" alt="Evologic Techonologies" /></a>
      </div>

      <input id="menu-toggle" type="checkbox" />
      <label class='menu-button-container' for="menu-toggle">
      <div class='menu-button'></div>
    </label>
      <ul class="menu-mobile">
        <li><a href="technology.php" class="mobile-nav">Technology</a></li>
        <li><a href="products.php" class="mobile-nav">Product</a></li>
        <li><a href="service.php" class="mobile-nav">Service</a></li>
        <li><a href="about.php" class="mobile-nav">About Us</a></li>
        <li><a href="investor.php" class="mobile-nav">Investor Relations</a></li>
      </ul>
  </section>



  </div>
  <div class="header-separator" id="header-separator-rnd"></div>

  <div data-p="225.00">
    <img data-u="image" src="img/technology-banner1.png" class="banner-img2" />
    <img data-u="image" src="img/transition.png" class="banner-transition-tech" />
  </div>
   <div class="banner-text-3-tech">

    <div class="banner-text-small-3-technology22">Our patented platform technology allows to grow hairy roots in industrial scale bioreactors.</div>
  </div>

  <div class="content-technology">


    <div class="sidenav">
      <div class="technology-img-top">
        <img src="img/icons/technology1.png" alt="" class="technology-img-left">

      </div>
      <ul>
        <li class="actives ">
          <a href="#11" class="scroll" >Valuable Plant Extracts</a>
        </li>
        <li class="acc ">
          <a href="#22" class="scroll hairy" >Hairy roots</a>
        </li>
        <li class="acc ">
          <a href="#33" class="scroll handle" >Novel Production</a>
        </li>
        <li class="acc ">
          <a href="#44" class="scroll handle" >Outlook</a>
        </li>

      </ul>
    </div>

    <div class="artical">

      <div class="short-intro-about-scientific" id="11">
        <h2 class="short-intro-h2-products-hairy">Valuable Plant Extracts - Secondary Metabolites</h2>
        <p class="short-intro-p-technology">Various active pharmaceutical ingredients (API) have been discovered in plants. Those ingredients are produced by the plant as secondary metabolites and have a use not only as pharmaceuticals but also e.g. nutraceuticals, fragrance or flavoring essence. Today, the less complex compounds are commonly produced by chemical synthesis (e.g. vanillin) or by microbial bioprocesses. However, the most complex ones are still sourced from the plant, even in the 21st Century. Hairy roots allow to produce the relevant compounds as natural product using a bioreactor instead of vast areas of land.</p>

      </div>

    <div class="short-intro-about-scientific" id="22">
      <h2 class="short-intro-h2-products-hairy">Plants in a Steel Tank - Hairy Roots (HR)</h2>
      <p class="short-intro-p-technology">Hairy roots are the result of a plant tissue transformation with agrobacillus. In a natural process the tissue starts growing in form of roots that feature a large potential for the production of complex compounds. Given the natural transformation process, hairy roots are generally regarded as non-GMO organisms. Relevant compounds in hairy roots include rather complex secondary metabolites like terpenes, flavonoids and phenolic compounds. What makes the hair roots really stand out is the fact that they can be directly used, since they are not genetically modified and originate directly from nature. To this date, attempts for industrial scale hairy root production failed to scale above 70L volume despite the significant economic potential, Our novel production approach is based on a revolutionary, patented reactor design. This development allows the upscaling of the hairy root production process to several m3 for the first time.</p>

    </div>



    <div class="short-intro-about-scientific">
      <h2 class="short-intro-h2-phytotechnicals">Potential products</h2>
      <p class="short-intro-p">Products produced in a 21st century HR reactor include compounds from the family of phenols,
flavonoids and terpenes.
</p>
    </div>

    <div class="pages-wrapper middle">

      <div class="mainlogos-technology">

        <div class="logos2 ll">
          <img class="logos-img  ch-logos" src="img/icons/c-1.png" alt="Products" style="width:200px;">
          <p class="application-technology">Phenolic Compounds</p>
          <!-- <div class="overlay2"></div> -->
          <!-- <div class="button"><a href="products.php"> Read More! </a></div> -->
        </div>
        <div class="logos3 ll">
          <img class="logos-img ch-logos" src="img/icons/c-2.png" alt="About Us" style="width:150px;">
          <p class="company-technology">Flavoniods</p>
          <!-- <div class="overlay3"></div> -->
          <!-- <div class="button"><a href="about.php"> Read More! </a></div> -->
        </div>

        <div class="logos4 ll">
          <img class="logos-img ch-logos" src="img/icons/c-3.png" alt="About Us" style="width:170px;">
          <p class="company-technology">Terpenes</p>
          <!-- <div class="overlay3"></div> -->
          <!-- <div class="button"><a href="about.php"> Read More! </a></div> -->
        </div>
      </div>
    </div>

    <div class="short-intro-about-scientific">
      <h2 class="short-intro-h2-products" id="nutrient-delivery-tech3333">Our patented platform technology allows us to grow hairy roots (HR) in industrial scale bioreactors</h2><img data-u="image" src="img/transition.png" class="banner-transition-tech2-products-technology443"><br><br><br>

      <h2 class="short-intro-h2-technology" id="33" >Novel HR Production Approach</h2>
      <img src="img/bioreactors_alu.jpg" id="img-tech" alt="">
      <p class="short-intro-p">Evologic built and tested a series of bioreactor prototypes to tackle the critical issues concerning the scale up. Our novel reactor design enabled us to overcome the challenges on the way to industrial scale while providing highly reproducible batch cultivations. Currently the requirements for the realization of a seed are under development.</p>

      <p class="short-intro-p">Before the creation of the latest bioreactor design, the geometric dimensions and resulting flow patterns have been optimized in silico. Using a computational flow dynamics (CFD) model, flow patterns and particle behavior is modeled an analyzed. A regular verification of the CFD model with experimental data ensures the validity of our models. This approach allows using algorithm-based design optimization and helps to save time and costs over common trial an error approach. </p>
    </div>

    <div class="short-intro-about-scientific">
      <h2 class="short-intro-h2-technology" id="44">Outlook</h2>
      <p class="short-intro-p">Within the next two years the scale of our hairy root production as well as the mycorrhiza production will be 15 m3. This will allow us to decrease the cost of goods for mycorrhiza dramatically and thereby pave the way for us to enter the market with our product.</p>
    </div>


  </div>


</div>
  <div class="footer">
      <div class="footer-separator"></div>
      <div class="footer-left">
        <p class="contact">Contact:</p>
        <p class="buro">office +43 324324856 </p>
        <a class="office" href="mailto:office@evologic.at?subject=feedback" target="_blank">office@evologic.at</a><br>
        <a class="www" href="http://www.evologic.at" target="_blank"> www.evologic.at</a>
      </div>
      <div class="footer-right">
        <ul class="ul-footer">
          <li class="home"><a class="home" href="http://www.evologic.at" target="_blank">Home</a></li>
          <li><a class="agb" href="agb.php" target="_blank">AGB</a></li>
          <li class="impli"><a class="imp" href="impressum.php" target="_blank">Impressum</a></li>
        </ul>
      </div>
  </div>

  <script>

      var elements = document.getElementsByClassName("child");
      for(var i = 0; i < elements.length; i++)
      {
          elements[i].onclick = function(){

              // remove class from sibling

              var el = elements[0];
              while(el)
              {
                  if(el.tagName === "DIV"){
                      //remove class
                      el.classList.remove("bak");

                  }
                  // pass to the new sibling
                  el = el.nextSibling;
              }

            this.classList.add("bak");
          };
      }

  </script>
<script type="text/javascript">jssor_1_slider_init();</script>
<script type="text/javascript" src="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.min.js"></script>
<script src="js/jquery-2.1.4.js"></script>
<script src="js/jquery.mobile.custom.min.js"></script>
<script src="js/main.js"></script> <!-- Resource jQuery -->
<script src="js/js/smooth.js"type="text/javascript"></script>
<script src="js/top.js" type="text/javascript"></script>
</body>
</html>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Evologic - Grow. Different.</title>
  <link rel="icon" href="img/favicon.jpg" type="image/gif" sizes="16x16">
  <link rel="stylesheet" type="text/css" href="css/reset.css">
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <link rel="stylesheet" type="text/css" href="css/slider.css">
  <script src="js/jssor.slider-27.1.0.min.js" type="text/javascript"></script>
  <script src="js/slider.js" type="text/javascript"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="js/scroll.js" type="text/javascript"></script>
  <script src="js/ss.js" type="text/javascript"></script>
  <script src="js/kk.js"type="text/javascript"></script>
  <script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
  <script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.css"/>
  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick-theme.css"/>
  <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300italic,regular,italic,700,700italic&subset=latin-ext,greek-ext,cyrillic-ext,greek,vietnamese,latin,cyrillic" rel="stylesheet" type="text/css" />
  <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Cabin:400,700" rel="stylesheet">
</head>
<body>
  <a href="javascript:" id="return-to-top"><i class="icon-chevron-up"></i></a>
  <div class="menu-wrapper-home">
   <div class="menu">
      <a href="http://www.evologic-technologies.com/" id="logo-link"><img src="img/logo1.png" alt="Evologic Techonologies" /></a>
      <div class="menu-entries top" id="menu-entries" >
          <!-- <div id="menu-entry-1" class="tooltip hov"> -->
              <a href="technology.php" class="main-nav">Technology</a>
          <!-- </div> -->
          <!-- <div  id="menu-entry-2" class="tooltip2 hov"> -->
              <a href="products.php" class="main-nav">Product</a>
          <!-- </div> -->
          <!-- <div  id="menu-entry-3" class="tooltip3 hov"> -->
              <a href="service.php" class="main-nav">Service</a>
          <!-- </div> -->
          <!-- <div  id="menu-entry-3" class="tooltip3 hov"> -->
              <a href="about.php" class="main-nav">About Us</a>
          <!-- </div> -->
          <!-- <div id="menu-entry-3" class="tooltip3 hov"> -->
              <a href="investor.php" class="main-nav">Investor Relations</a>
          <!-- </div> -->
      </div>
      <div class="clear"></div>
    </div>

    <section class="top-nav">
      <div>
        <a href="http://www.evologic-technologies.com/" id="logo-link"><img src="img/logo1.png" alt="Evologic Techonologies" /></a>
      </div>

      <input id="menu-toggle" type="checkbox" />
      <label class='menu-button-container' for="menu-toggle">
      <div class='menu-button'></div>
    </label>
      <ul class="menu-mobile">
        <li><a href="technology.php" class="mobile-nav">Technology</a></li>
        <li><a href="products.php" class="mobile-nav">Product</a></li>
        <li><a href="service.php" class="mobile-nav">Service</a></li>
        <li><a href="about.php" class="mobile-nav">About Us</a></li>
        <li><a href="investor.php" class="mobile-nav">Investor Relations</a></li>
      </ul>
  </section>



  </div>
  <div class="header-separator"></div>

  <div class="content">
    <h3 class="agb-h31">Terms of Service</h3><br>
    <hr>
    <h3 class="agb-h3">Scope, written form:</h3><br>
    <p class="agb-p">The company Evologic Technologies operates exclusively on the basis of these terms and conditions. These terms and conditions are an integral part of any contract concluded by Evologic Technologies. The respective contractual partner of Evologic Technologies will be referred to in the following as the "contractual partner". Agreements other than those below will not be considered unless expressly stated in writing. Verbal changes, subsidiary agreements or assurances require the written confirmation of the legally authorized representatives of Evologic Technologies registered in the commercial register. Any conditions of the contracting party, which contradict the existing general terms and conditions are void, if not otherwise mutually agreed on - applying the above mentioned formal written framework.</p>
    <br>
    <h3 class="agb-h3">2. Conclusion of the contract:</h3><br>
    <p class="agb-p">The order of the contracting party is a binding offer. Evologic Technologies may choose to accept this offer within 3 days by sending an order confirmation, or by sending the ordered goods to the other party within the agreed deadlines.</p><br>
    <h3 class="agb-h3">3. Prices and terms of payment:</h3><br>
    <p class="agb-p">The prices according to the contract are generally valid in euros or in an otherwise agreed on currency and apply, unless otherwise agreed, ex delivery warehouse / factory of Evologic Technologies. The prices are, unless explicitly agreed otherwise in writing, gross prices including VAT.
    <br><br>Invoices of the company Evologic Technologies are due for payment without deduction after receipt, unless another term of payment has been agreed upon. Payments to Evologic Technologies only have a debt discharging effect, if they are made by bank transfer to the bank account specified in the invoice. The day of receipt of payment is the date on which the payment actually arrived at Evologic Technologies and, in the case of a bank transfer, the day of the booking at the Evologic Technologies bank. All associated fees, costs and expenses shall be borne by the contracting party. The issue of own or third-party bills of exchange does not yet constitute a payment and does not justify any claim for a possible discount. If the contractual partner is an entrepreneur, the retention of payments due to asserted warranty claims is not permitted. Warranty claims therefore do not postpone the due date of the payment. If a cash discount has been agreed upon, the cash discount period begins when the invoice is sent to Evologic Technologies. Any warranty claims or alleged counterclaims do not interrupt or inhibit the discount period. Evologic Technologies is entitled to use incoming payments regardless of any other dedication at its discretion for any obligations of any kind, including reminder fees for prosecution of claims, expenses, residence expenses, interest and accordingly default interest, and at last, principal amounts. If the contracting party is an entrepreneur, he is bound to, in the event of default, fully reimburse all dunning charges, costs and cash expenses associated with the collection of the purchase price, so that no costs under any circumstances may arise for Evologic Technologies from the collection of its claim. The contracting partner is not entitled to set off his claims against the claims of Evologic Technologies. The contracting party may assign claims from the contractual relationship with Evologic Technologies to third parties only with the express consent in accordance with the formal requirements of section II. 2. For partial payments, deadline loss is agreed. In the case of late or incomplete payment of even a partial payment, the entire remaining outstanding claim will become due in full immediately.
  </p><br>
    <h3 class="agb-h3">Terms of delivery:</h3><br>
    <p class="agb-p">The delivery period begins with the receipt of the order or, if agreed, with dispatch of the order confirmation by Evologic Technologies, but not before receipt and clarification of all documents by the contracting partner. The adherence of the delivery period presupposes the fulfillment of all contractual obligations of the contracting party, in particular the observance of the agreed terms of payment.</p>
    <p class="agb-p">The delivery period is deemed to have been met if the shipment is ready for dispatch within the agreed period or, if agreed, delivered.</p>
    <p class="agb-p">Partial deliveries are permitted. The waiting times of the contractual partner associated with partial or incorrect deliveries do not entitle the contractual partner to withdraw from the contract or to assert claims for damages (loss of earnings and the like), unless Evologic Technologies gives its express written consent to offset such costs. If the delivery period is culpably exceeded by Evologic Technologies, the contractual partner must provide a reasonable grace period by registered letter. The delivery period shall be extended appropriately in the event of industrial disputes, in particular strikes and lockouts, as well as in the event of unforeseen impediments beyond the control of Evologic Technologies, insofar as such impediments are proven to have a measurable impact on the fulfilling of the entire contract or part of the contract. This also applies if the circumstances occur with subcontractors. The above circumstances are not the responsibility of Evologic Technologies, even if they occur during an already existing default. The beginning and end of such obstacles will, in important cases, be communicated by Evologic Technologies to the other party as soon as possible.
    </p><br>
    <h3 class="agb-h3">Retention of title:</h3><br>
    <p class="agb-p">All goods remain the property of Evologic Technologies until full payment of the agreed price plus ancillary charges are made. As long as there are any claims arising out of contracts between Evologic Technologies and the contractor, the contractor may not sell, rent, lend, pledge, give away or transfer the goods abroad without the consent of Evologic Technologies. Evologic Technologies has the right to assure itself at any time of the presence and condition of the goods until full payment of its claim.</p><br>
    <p class="agb-p">If the goods are seized by third parties, the contracting party is obliged to notify Evologic Technologies immediately by registered letter. To be Included is the seizure protocol and the affidavit of the content, that the seized goods are identical to the delivered goods by Evologic Technologies and not yet paid. In the event of bankruptcy or settlement, the contracting party is obliged to immediately notify Evologic Technologies and to discard all items and claims subject to retention of title. The intervention costs are charged to the contracting partner. The seizure of goods delivered under retention of title on behalf of Evologic Technologies shall not be regarded as a waiver of the reservation of title. In the event Evologic Technologies makes use of its reserved property, the contracting partner hereby already gives its irrevocable consent that Evologic Technologies may pick up the delivered goods at any time without the further consent of the contracting party.
    </p><br>

    <p class="agb-p">Any resale of goods subject to retention of title is only permissible under upkeep of the reservation of title. Evologic Technologies is entitled to sell the returned goods to third parties without checking the appropriateness of the purchase price. The proceeds from the sale of the returned items shall be deducted from the original price claim. The contracting party shall reimburse Evologic Technologies for the resulting amount plus interest and all expenses associated with the enforcement of the retention of title, or shall invoice it against the purchase price already paid.</p><br>
    <h3 class="agb-h3">4.Warranty:</h3><br>
    <p class="agb-p">In case of other loss of warranty claims the contractual partner must check the goods carefully within 3 days after receipt, and notify Evologic Technologies about the defect in writing. Evologic Technologies may, in the case of a generic debt, exempt itself from the claims for termination of the contract or for reasonable price reduction by exchanging the defective goods for a defect-free product within a reasonable period of time. Further warranty claims are excluded. If Evologic Technologies offers to exchange the goods and if the contractual partner does not allow the offered replacement within a reasonable period of time, Evologic Technologies is exempted from any warranty. In particular, the contractual partner is not entitled to charge the costs of a replacement procurement to Evologic Technologies. For goods purchased from subcontractors by Evologic Technologies, Evologic Technologies may, at its discretion, either warrant under the warranty provided under this point or assign its warranty claims against the supplier to the contractual partner. In the latter case, Evologic Technologies is exempt from any warranty and compensation claims.
    </p><br>
    <h3 class="agb-h3">5. Transfer:</h3><br>
    <p class="agb-p">The delivery of the goods takes place, unless otherwise agreed, on account of the contracting partner. Evologic Technologies will seek to meet the exact delivery dates. The contractual partner cannot derive any claims from the postponement of dates.</p><br>
    <h3 class="agb-h3">6. Compensation, penalties:</h3><br>
    <p class="agb-p">For any damages or any agreed contractual penalty, Evologic Technologies shall be liable only if gross negligence or willful misconduct is proven by the contracting party. If the contracting party is a consumer, this limitation of liability does not apply to personal injury. In addition, Evologic Technologies shall not be liable to the contracting party for lost profits or other financial losses. The delivered goods offer only the security which can be expected on the basis of general regulations or specifications of the subcontractors. In the case of legal product liability, liability for damage to property is limited to such damage suffered by consumers. The limitation of liability shall be fully transferred to any purchasers with the obligation of continued transfer.</p><br>
    <h3 class="agb-h3">Resignation Evologic Technologies, fine for breach of contract:</h3><br>
    <p class="agb-p">Evologic Technologies is entitled to withdraw from the contract in the event of default by the contractor without granting a grace period. In this case, Evologic Technologies is entitled to assert a penalty of 20% of the agreed total price. Any further damage shall also be reimbursed by the contractual partner, in particular if Evologic Technologies has already produced goods or their production has already started, or these goods are already ready for dispatch, if the damage exceeds the aforementioned 20% of the agreed total price. In addition, Evologic Technologies is entitled to withdraw from the contract if, after the legal validity of the contract, circumstances become known which raise doubts about the fulfillment of the contract by the contractual partner. In the case of such a declaration of withdrawal, the contracting party is not entitled to any claim.</p><br>
    <h3 class="agb-h3">Transport damage:</h3><br>
    <p class="agb-p">In the event of any damage in transit, the issuance of a damage report (train, post, freight forwarder) must be produced immediately. The goods are to be checked immediately upon acceptance for any transport damage. In case of damage, the type of damage to the bill of lading must be confirmed by the carrier. If the return of the goods reveals that the claims have been made unjustifiably, Evologic Technologies shall be entitled to charge for the costs of shipping and an appropriate fee for the inspection.</p><br>
    <h3 class="agb-h3">Returns:</h3><br>
    <p class="agb-p">Returned goods are only accepted, if an agreement with Evologic Technologies has been made in advance.</p><br>
    <h3 class="agb-h3">7. Place of fulfillment, Jurisdiction, Applicable Law:</h3><br>
    <p class="agb-p">The place of performance is the registered office of Evologic Technologies for both parties. The parties to the contract shall agree on the application of Austrian law and the local jurisdiction of the relevant court in Vienna, Austria, for all disputes arising from this contract.</p><br>
    <h3 class="agb-h3">Data protection</h3><br>
    <p class="agb-p">The operators of these sites take the protection of your personal data very seriously. We treat your personal data confidentially and in accordance with the statutory data protection regulations and this privacy policy.</p><br>
    <p class="agb-p">The use of our website is usually possible without providing personal information. The collection of any personal data (such as name, address or email addresses) is made, as far as possible, on a voluntary basis. These data will not be disclosed to third parties without your explicit consent.</p><br>
    <p class="agb-p">Please note that data transmission over the Internet (for example, when communicating via e-mail) may have security vulnerabilities. A complete protection of the data from access by third parties is not possible.</p><br>
    <h3 class="agb-h3">Cookies</h3><br>
    <p class="agb-p">The internet pages partly use so-called cookies. Cookies do not harm your computer and do not contain viruses. Cookies serve to make our offer more user-friendly, effective and secure. Cookies are small text files that are stored on your computer and stored by your browser.</p><br>
    <p class="agb-p">Most of the cookies we use are so-called "session cookies". They are automatically deleted after your visit. Other cookies remain stored on your device until you delete them. These cookies allow us to recognize your browser the next time you visit.</p><br>
    <p class="agb-p">You can set your browser so that you are informed about the setting of cookies and allow cookies only in individual cases, the acceptance of cookies for certain cases or generally exclude and activate the automatic deletion of cookies when closing the browser. Disabling cookies may limit the functionality of this website.</p><br>
    <p class="agb-p">Privacy Policy for the use of Google Analytics</p><br>
    <p class="agb-p">This website uses functions of the web analytics service Google Analytics. Provider is Google Inc., 1600 Amphitheater Parkway Mountain View, CA 94043, USA.</p><br>
    <p class="agb-p">Google Analytics uses so-called "cookies". These are text files that are stored on your computer and that allow an analysis of the use of the website by you. The information generated by the cookie about your use of this website is usually transmitted to a Google server in the USA and stored there.</p><br>
    <p class="agb-p">For more information on how to handle user data on Google Analytics, please refer to the Google Privacy Policy: https://support.google.com/analytics/answer/6004245?hl=en</p><br>
    <h3 class="agb-h3">Browser Plugin</h3><br>
    <p class="agb-p">You can prevent the storage of cookies by a corresponding setting of your browser software; however, please note that if you do this, you may not be able to use all the features of this website to the fullest extent possible. In addition, you may prevent the collection by Google of the data generated by the cookie and related to your use of the website (including your IP address) as well as the processing of this data by Google by downloading and installing the browser plug-in available under the following link: https://tools.google.com/dlpage/gaoptout?hl=en</p><br>
    <h3 class="agb-h3">Opposition to data collection</h3><br>
    <p class="agb-p">You can prevent the collection of your data by Google Analytics by clicking on the following link. An opt-out cookie will be set which prevents the collection of your data on future visits to this website: disable Google Analytics</p><br>
    <p class="agb-p">Privacy Policy for the use of Facebook plugins (like button)</p><br>
    <p class="agb-p">On our webpages plugins of the social network Facebook, provider Facebook Inc., 1 Hacker Way, Menlo Park, California 94025, USA, are integrated. The Facebook plugins can be recognized by the Facebook logo or the "Like-Button" ("Like") on our site. An overview of the Facebook plugins can be found here: http://developers.facebook.com/docs/plugins/.</p><br>
    <p class="agb-p">When you visit our pages, the plugin establishes a direct connection between your browser and the Facebook server. Facebook receives the information that you have visited our site with your IP address. If you click on the Facebook "Like-Button" while you are logged in to your Facebook account, you can link the contents of our pages to your Facebook profile. As a result, Facebook can assign the visit to our pages to your user account. We point out that we as the provider of the pages are not aware of the content of the data transmitted and their use by Facebook. For more information, see the Facebook Privacy Policy at http://www.facebook.com/policy.php.</p><br>
    <p class="agb-p">If you do not wish Facebook to associate your visit to our pages with your Facebook user account, please log out of your Facebook user account.</p><br>
  </div>


  <div class="footer">
      <div class="footer-separator"></div>
      <div class="footer-left">
        <p class="contact">Contact:</p>
        <p class="buro">office +43 324324856 </p>
        <a class="office" href="mailto:office@evologic.at?subject=feedback" target="_blank">office@evologic.at</a><br>
        <a class="www" href="http://www.evologic.at" target="_blank"> www.evologic.at</a>
      </div>
      <div class="footer-right">
        <ul>
          <li class="home"><a class="home" href="http://www.evologic.at" target="_blank">Home</a></li>
          <li><a class="agb selected-nav" href="agb.php" target="_blank">AGB</a></li>
          <li class="impli"><a class="imp" href="impressum.php" target="_blank">Impressum</a></li>
        </ul>
      </div>
  </div>

  <script>

      var elements = document.getElementsByClassName("child");
      for(var i = 0; i < elements.length; i++)
      {
          elements[i].onclick = function(){

              // remove class from sibling

              var el = elements[0];
              while(el)
              {
                  if(el.tagName === "DIV"){
                      //remove class
                      el.classList.remove("bak");

                  }
                  // pass to the new sibling
                  el = el.nextSibling;
              }

            this.classList.add("bak");
          };
      }

  </script>
<script type="text/javascript">jssor_1_slider_init();</script>
<script type="text/javascript" src="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.min.js"></script>
<script src="js/top.js" type="text/javascript"></script>
</body>
</html>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Evologic - Grow. Different.</title>
  <link rel="icon" href="img/favicon.jpg" type="image/gif" sizes="16x16">
  <link rel="stylesheet" type="text/css" href="css/reset.css">
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <link rel="stylesheet" type="text/css" href="css/slider.css">
  <script src="js/jssor.slider-27.1.0.min.js" type="text/javascript"></script>
  <script src="js/slider.js" type="text/javascript"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="js/scroll.js" type="text/javascript"></script>
  <script src="js/ss.js" type="text/javascript"></script>
  <script src="js/kk.js"type="text/javascript"></script>

  <script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.css"/>
  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick-theme.css"/>
  <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300italic,regular,italic,700,700italic&subset=latin-ext,greek-ext,cyrillic-ext,greek,vietnamese,latin,cyrillic" rel="stylesheet" type="text/css" />
  <link href='https://fonts.googleapis.com/css?family=Playfair+Display:700,900|Fira+Sans:400,400italic' rel='stylesheet' type='text/css'>
  <link href="https://fonts.googleapis.com/css?family=Cabin:400,700" rel="stylesheet">
  <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
	<link rel="stylesheet" href="css/timeline.css"> <!-- Resource style -->
	<script src="js/modernizr.js"></script> <!-- Modernizr -->
</head>
<body>
  <a href="javascript:" id="return-to-top"><i class="icon-chevron-up"></i></a>
  <div class="menu-wrapper">
   <div class="menu">
      <a href="http://www.evologic-technologies.com/testsite" id="logo-link"><img src="img/logo1.png" alt="Evologic Techonologies" /></a>
      <div class="menu-entries" id="menu-entries-rnd" >
          <div onclick="location.href = 'r&d.php';" id="menu-entry-1" class="tooltip bak">
              <span class="bold">R&D</span>
          </div>
          <div  onclick="location.href = 'products.php';" id="menu-entry-2" class="tooltip2 ">
              <span class="bold">Products</span>
          </div>
          <div onclick="location.href = 'about.php';"id="menu-entry-3" class="tooltip3 ">
              <span class="bold">About Us</span>
          </div>
      </div>
      <div class="clear"></div>
    </div>
  </div>
  <div class="header-separator" id="header-separator-rnd"></div>

  <div class="content-about">
    <div class="sidenav">
      <ul>
        <li class="actives ">
          <a href="#what" class="scroll" >Approach</a>
        </li>
        <li class="acc ">
          <a href="#principles" class="scroll" >Principles</a>
        </li>
          <li class="acc " id="li-right">
            <a href="#nutrient-delivery" class="scroll">Automation</a>
          <li>
          <li class="acc " id="li-right">
            <a href="#water-saving" class="scroll">Parallelisation</a>
          </li>
          <li class="acc " id="li-right">
            <a href="#whereis" class="scroll">Scalability</a>
          </li>
          <li class="acc " id="li-right">
            <a href="#succeed" class="scroll">Focus on the producers</a>
          </li>

      </ul>
    </div>

    <div class="short-intro-about-scientific" id="what">
      <h2 class="short-intro-h2-products">Approach</h2>
      <p class="short-intro-p">We believe that a economical advantage with a given product can only be achieved and maintained based on a know-how advantage. This requires a comprehensive understanding of a given process.
 To obtain this level of insight a lot of high quality data (real time!) is necessary. But to make use of this data and the invested effort, this data must not simply fill Excel sheets but requires comprehensive data analysis across experiments. This is why we try to invest at least 50% of the effort into planning and evaluation of our experiments, to simply get the most from the carefully collected data.
      <br>To maximize the effectivity of bioprocess development and scaleup we follow 4 principles: </p>
      <ul  id="short-intro-ul-li">
        <li class="li-img"><p class="short-intro-p-left-up">Automation - highly repetitive steps can often be completed a pretty stupid robot</p></li>
        <li class="li-img"><p class="short-intro-p-left-up">Parallelization - to maximize the experimental throughput by combining workflows</p></li>
        <li class="li-img"><p class="short-intro-p-left-up">Scalability - scalability is not a nice to have - but a paradigm from the start</p></li>
        <li class="li-img"><p class="short-intro-p-left-up">Focus on the producers - the cells the content is productive not the reactor</p></li>
      </ul>
    </div>


    <div class="short-intro-about-scientific" id="principles">
      <h2 class="short-intro-h2-products">Principles</h2>
      <h2 class="short-intro-h2-products" id="nutrient-delivery">Automation</h2>
      <p class="short-intro-p">A lot of bench work is cumbersome and highly repetitive.  Although we all enjoy working at the bench we prefer the excitement of doing novelty rather than repetitive work. Generally spoken there are more demanding tasks for humans than pipetting the same volume over and over again - like data analysis. This doesn´t only apply for analytical assays but also for many sampling steps during the bioprocess. Nevertheless, to effort has to be directed towards the most pressing issues. Commonly, we prioritize in respect to the contribution of the step to the total error of measurement. </p>
      <img src="img/1111.png" id="img-1111" alt="">
      <p class="short-intro-p">Therefore we constantly strive to find simple solutions to increase reproducibility. In this context the solution doesn´t have to come with a impressive price tag, sometimes a simple script and a old liquid handler can do the job - repetitively accurate.</p>
      <!-- <img src="img/3333.png" id="img-3333" alt=""> -->
    </div>

    <div class="short-intro-about-scientific">
      <h2 class="short-intro-h2-products" id="water-saving">Parallelisation</h2>
      <p class="short-intro-p">The idea of parallelization is lots of things but it´s definitely not new. Already Henry Ford used the synergies which occur if process steps are grouped and executed within a assembly line. In this respect, the parallelization of e.g. reactors allows to minimize the necessary time for setup/cleaning. Instead of executing steps as e.g. media preparation, sterilisation,... 4 times in a row, these steps can be combined which saves a lot of time and increases reproducibility. </p>
      <img src="img/5555.png" id="img-5555" alt="">
    </div>

    <div class="short-intro-about-scientific">
      <h2 class="short-intro-h2-products" id="whereis">Scalability</h2>
      <p class="short-intro-p">In the lab, a lot of improvised solutions can really speed things up. But it is of utmost importance that a given process is not being optimized to perfection using a variety of custom made solutions. Most importantly in bioprocess development scalability must always stay in focus. As soon as during the experimental design for lab scale we constrain the design-space of process variables to the technical feasible space in target production scale. Otherwise, relatively simple laws of physics can turn out to be a real dealbreaker (e.g. heattransfer, oxygentransfer).</p>
      <img src="img/6666.png" id="img-6666" alt="">
    </div>

    <div class="short-intro-about-scientific-last-products succeed" >
      <h2 class="short-intro-h2-products" id="succeed" >Focus on the producers</h2>
      <p class="short-intro-p">In a bioprocess the most decisive factor for high productivity are the producers - the organism/cells. In contrast to a long history of regarding the "bioreactor" as productive entity, state of the art technologies allow us today to take a closer look at a cellular level.</p>
      <img src="img/7777.png" id="img-7777" alt="">
    </div>
  </div>



  <div class="footer">
      <div class="footer-separator"></div>
      <div class="footer-left">
        <p class="contact">Contact:</p>
        <p class="buro">office +43 324324856 </p>
        <a class="office" href="mailto:office@evologic.at?subject=feedback" target="_blank">office@evologic.at</a><br>
        <a class="www" href="http://www.evologic.at" target="_blank"> www.evologic.at</a>
      </div>
      <div class="footer-right">
        <ul class="ul-footer">
          <li class="home"><a class="home" href="http://www.evologic.at" target="_blank">Home</a></li>
          <li><a class="agb" href="agb.php" target="_blank">AGB</a></li>
          <li class="impli"><a class="imp" href="impressum.php" target="_blank">Impressum</a></li>
        </ul>
      </div>
  </div>

  <script>

      var elements = document.getElementsByClassName("child");
      for(var i = 0; i < elements.length; i++)
      {
          elements[i].onclick = function(){

              // remove class from sibling

              var el = elements[0];
              while(el)
              {
                  if(el.tagName === "DIV"){
                      //remove class
                      el.classList.remove("bak");

                  }
                  // pass to the new sibling
                  el = el.nextSibling;
              }

            this.classList.add("bak");
          };
      }

  </script>
<script type="text/javascript">jssor_1_slider_init();</script>
<script type="text/javascript" src="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.min.js"></script>
<script src="js/jquery-2.1.4.js"></script>
<script src="js/jquery.mobile.custom.min.js"></script>
<script src="js/main.js"></script> <!-- Resource jQuery -->
<script src="js/js/smooth.js"type="text/javascript"></script>
<script src="js/top.js" type="text/javascript"></script>
</body>
</html>

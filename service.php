<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Evologic - Grow. Different.</title>
  <link rel="icon" href="img/favicon.jpg" type="image/gif" sizes="16x16">
  <link rel="stylesheet" type="text/css" href="css/reset.css">
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <link rel="stylesheet" type="text/css" href="css/slider.css">
  <script src="js/jssor.slider-27.1.0.min.js" type="text/javascript"></script>
  <script src="js/slider.js" type="text/javascript"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="js/scroll.js" type="text/javascript"></script>
  <script src="js/ss.js" type="text/javascript"></script>
  <script src="js/kk.js"type="text/javascript"></script>

  <script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.css"/>
  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick-theme.css"/>
  <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300italic,regular,italic,700,700italic&subset=latin-ext,greek-ext,cyrillic-ext,greek,vietnamese,latin,cyrillic" rel="stylesheet" type="text/css" />
  <link href='https://fonts.googleapis.com/css?family=Playfair+Display:700,900|Fira+Sans:400,400italic' rel='stylesheet' type='text/css'>
  <link href="https://fonts.googleapis.com/css?family=Cabin:400,700" rel="stylesheet">
  <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
	<link rel="stylesheet" href="css/timeline.css"> <!-- Resource style -->
	<script src="js/modernizr.js"></script> <!-- Modernizr -->
</head>
<body>
  <a href="javascript:" id="return-to-top"><i class="icon-chevron-up"></i></a>
  <div class="menu-wrapper-home">
   <div class="menu">
      <a href="http://www.evologic-technologies.com/" id="logo-link"><img src="img/logo1.png" alt="Evologic Techonologies" /></a>
      <div class="menu-entries top" id="menu-entries" >
          <!-- <div id="menu-entry-1" class="tooltip hov"> -->
              <a href="technology.php" class="main-nav">Technology</a>
          <!-- </div> -->
          <!-- <div  id="menu-entry-2" class="tooltip2 hov"> -->
              <a href="products.php" class="main-nav">Product</a>
          <!-- </div> -->
          <!-- <div  id="menu-entry-3" class="tooltip3 hov"> -->
              <a href="service.php" class="main-nav selected-nav">Service</a>
          <!-- </div> -->
          <!-- <div  id="menu-entry-3" class="tooltip3 hov"> -->
              <a href="about.php" class="main-nav">About Us</a>
          <!-- </div> -->
          <!-- <div id="menu-entry-3" class="tooltip3 hov"> -->
              <a href="investor.php" class="main-nav">Investor Relations</a>
          <!-- </div> -->
      </div>
      <div class="clear"></div>
    </div>

    <section class="top-nav">
      <div>
        <a href="http://www.evologic-technologies.com/" id="logo-link"><img src="img/logo1.png" alt="Evologic Techonologies" /></a>
      </div>

      <input id="menu-toggle" type="checkbox" />
      <label class='menu-button-container' for="menu-toggle">
      <div class='menu-button'></div>
    </label>
      <ul class="menu-mobile">
        <li><a href="technology.php" class="mobile-nav">Technology</a></li>
        <li><a href="products.php" class="mobile-nav">Product</a></li>
        <li><a href="service.php" class="mobile-nav">Service</a></li>
        <li><a href="about.php" class="mobile-nav">About Us</a></li>
        <li><a href="investor.php" class="mobile-nav">Investor Relations</a></li>
      </ul>
  </section>



  </div>
  <div class="header-separator" id="header-separator-rnd"></div>
  <!-- <div id="jssor_1" style="position:relative;top:130px;left:0px;width:1300px;height:360px;overflow:hidden;visibility:hidden;">
      <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
          <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="img/spin.svg" />
      </div>
      <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:1300px;height:340px;overflow:hidden;">
          <div data-p="225.00">
              <img data-u="image" src="img/service-banner1.jpg" class="banner-img" />
              <img data-u="image" src="img/transition.png" class="banner-transition" />
              <div class="banner-text-3">

                <div class="banner-text-small-3-technology">Profits from our expertise and infrastructure! <br>Our experts have of years of experience.</div>
              </div>
          </div>
      </div>
  </div> -->

    <div data-p="225.00">
        <img data-u="image" src="img/service-banner1.png" class="banner-img-service" />
        <img data-u="image" src="img/transition.png" class="banner-transition-service" />
        <div class="banner-text-3-service">

          <div class="banner-text-small-3-service-4">Profits from our expertise and infrastructure! <br>Our experts have of years of experience.</div>
        </div>
    </div>
      <div class="sidenav">
        <div class="technology-img-top">
          <img src="img/icons/service1.png" alt="" class="technology-img-left">

        </div>
        <ul>
          <li class="actives ">
            <a href="#11" class="scroll" >Project types</a>
          </li>
          <li class="acc ">
            <a href="#22" class="scroll hairy right-side" >Contract</a>
          </li>
          <li class="acc ">
            <a href="#33" class="scroll handle right-side" >Cooperation</a>
          </li>
          <li class="acc ">
            <a href="#44" class="scroll handle" >Project content</a>
          </li>
          <li class="acc ">
            <a href="#55" class="scroll handle right-side" >Feasibility</a>
          </li>
          <li class="acc ">
            <a href="#66" class="scroll handle right-side" >Process Dev.</a>
          </li>
          <li class="acc ">
            <a href="#77" class="scroll handle right-side" >Process Opt.</a>
          </li>
          <li class="acc ">
            <a href="#88" class="scroll handle" >Why us?</a>
          </li>
          <li class="acc ">
            <a href="#99" class="scroll handle right-side" >Reproducibility</a>
          </li>
          <li class="acc ">
            <a href="#10" class="scroll handle right-side" >Speed</a>
          </li>
          <li class="acc ">
            <a href="#1010" class="scroll handle right-side" >Scalability</a>
          </li>
          <li class="acc ">
            <a href="#011" class="scroll handle right-side" >Focus: Cells</a>
          </li>
        </ul>
      </div>
  <div class="content-service">


    <div class="short-intro-about-scientific" id="11">

      <p class="short-intro-p-technology">Our competitive advantage is the ability to transfer technologies and know-how from the field of red biotech into green biotech. During previous projects, the founding members implemented, developed and advanced cutting edge technologies for pharmaceutical production processes. Our close cooperation with workshops allows very efficient development cycles and quick prototyping.</p><p class="short-intro-p-technology">At the intersection of academic research and industrial applications we can effectively develop high tech solutions cost and time efficiently. Find out more about our development capabilities and rationales. As project basis we offer two types of contracts:</p>
    </div>

    <div class="short-intro-about-scientific" id="44">
      <h2 class="short-intro-h2-products" id="nutrient-delivery-service5">Project type - Basis for collaboration</h2><img data-u="image" src="img/transition.png" class="short-intro-h2-service-2"><br><br>
    </div>
    <div class="products-icons">
      <div class="product-66-2" id="66">
        <img src="img/icons/text1.png" alt="" class="img-product-icons">
        <h2 class="products-icons-vito">Contract development</h2>
        <p class="short-intro-p">Full control over IP related to the production process is key for you as customer? We offer contract development services with clearly defined In- and Outputs. Our fully equipped lab with cutting edge equipment enables us to conduct scientific root cause analysis of your problem statement. We will provide simplistic solutions that fit your industrial environment.</p>
      </div>
      <div class="product-77-3" id="77" >
        <img src="img/icons/cooperation.png" alt="" class="img-product-icons2-new">
        <h2 class="products-icons-vito">Cooperative Projects</h2>
        <p class="short-intro-p">Cooperation projects featuring partners with complementary know how are an opportunity both parties to advance and optimize novel technologies. Based on a shared IP perspective we are keen to apply our full expertise in bioengineering and bioprocess development to make your product a success.</p>
      </div>
    </div>




    <div class="short-intro-about-scientific" id="44" style="margin-top: 100px;">
      <h2 class="short-intro-h2-products" id="nutrient-delivery-service5">Project Scope - What we offer</h2><img data-u="image" src="img/transition.png" class="short-intro-h2-service-2"><br><br><br><br><br>
      <p class="short-intro-p">We offer scientific expertise as well as bioprocess development services for hairy roots and microbial hosts. Depending on your requirements/ stage of product development we offer three general packages. Contact us to discuss your needs and to receive a quote/proposal tailored to your specific needs.</p>
    </div>

    <div class="short-intro-about-scientific" id="55">
      <h2 class="short-intro-h2-phytotechnicals" id="">Feasibility study</h2>
      <p class="short-intro-p"><bold>Need:</bold>You have promising results for your products and want to address the topic of economic feasibility?</p>
      <p class="short-intro-p"><bold>Service:</bold>We offer the conceptualization of a production process. Based on theoretic yields an estimation of the cost of goods dependent on production scales and necessary purities is possible. Standard packages do not include experimental work, but strain engineering, screening experiments and media definition/optimization experiments can be offered individually.</p>
    </div>

    <div class="short-intro-about-scientific" id="66">
      <h2 class="short-intro-h2-phytotechnicals" id="">Process Development</h2>
      <p class="short-intro-p"><strong>Need:</strong>First experiments have shown the feasibility of the product. Now a bioprocess is required to allow small production batches and to grant the foundation for upscaling/ outsourcing to a contract manufacturer.</p>
      <p class="short-intro-p"><strong>Service:</strong>We offer a risk analysis to identify the most potent process parameters to leverage productivity. Upon the definition of the relevant variables, a design of experiment is performed (e.g. 2 level screening design). The necessary experiments are conducted in fully equipped small scale bioreactors with high sensitivity real time analytic. Upon completion of the necessary experiments the data sets are evaluated in response to product quantity and quality to rank the impact of the investigated factors. The experiments commonly allow yield calculations as well as evaluations of up-scaling efforts.</p>
    </div>

    <div class="short-intro-about-scientific" id="77">
      <h2 class="short-intro-h2-phytotechnicals" id="">Process optimization</h2>
      <p class="short-intro-p"><strong>Need:</strong>To get the most out of your production facility you want to tweak your process parameters while avoiding re-designing the production units.</p>
      <p class="short-intro-p"><strong>Service:</strong>We offer in depth analysis of your process to identify the available degrees of freedom for productivity optimization. Upon mutual definition of the relevant variables for investigation, we perform an experimental design (e.g. full factorial 3 levels) within the physiological and technical feasible space and conduct the necessary experiments. After data analysis a set of verification experiments will substantiate the location of the optimum. If desired, a tech transfer work package can be included to ensure the subsequent implementation at your production unit.</p>
    </div>

    <div class="short-intro-about-scientific why-us" id="88">
      <h2 class="short-intro-h2-products" id="nutrient-delivery-service5">Why us – what makes us different.</h2><img data-u="image" src="img/transition.png" class="short-intro-h2-service-2"><br><br><br><br>
      <p class="short-intro-p-serv">Our competitive advantage is the ability to transfer technologies and know-how from the field of red biotech into green/white biotech. During previous projects, the founding members implemented, developed and advanced cutting edge technologies for pharmaceutical production processes. This specific know-how is the core of the competitive edge. Our direct and close cooperation with workshops allows efficient development cycles and quick prototyping.</p>
      <p class="short-intro-p">We offer scientific excellence as many others do. But more importantly we turn scientific explanations into industrial solutions. Only transferable, implementable solutions allow to precipitate the benefits of the gained know how. To ensure highly efficient development routines Evologic follows 4 principles:</p>
    </div>

    <div class="short-intro-about-scientific" id="99">
      <h2 class="short-intro-h2-phytotechnicals" id="">Reproducibility through Automation</h2>
      <p class="short-intro-p">A lot of manual bench work is cumbersome and highly repetitive. As a matter of fact, humans are less reliable in repetitive tasks than machines. Generally spoken, there are more exciting tasks for humans than pipetting - like data analysis. Nevertheless, the decision which tasks to automate is delicate, since the effort for automatization is not to be underestimated. Commonly, we prioritize in respect to the contribution of the task to the total error of analysis to ensure an optimal effort/information ratio.</p>
    </div>

    <div class="short-intro-about-scientific" id="10">
      <img src="img/auto.png" alt="" class="technology-img-left-3">
      <h2 class="short-intro-h2-phytotechnicals" id="">Speed - Parallelization</h2>
      <p class="short-intro-p">Already Henry Ford used the synergies which occur if process steps are grouped and executed within an assembly line. In this respect, the parallelization of e.g. reactors allows to minimize the necessary time for setup and cleaning. Instead of executing steps as e.g. media preparation, sterilization, several times in a in a row, these steps can be combined if reactors are set up in parallel. This saves a lot of time which can be invested otherwise and helps to increase efficiency.</p>
    </div>

    <div class="short-intro-about-scientific" id="1010">
      <img src="img/par.png" alt="" class="technology-img-left-3">
      <h2 class="short-intro-h2-phytotechnicals" id="">Scalability - Scale up vs. Number up </h2>
      <p class="short-intro-p">In the lab, a lot of improvised solutions can really speed things up. But it is of utmost importance that a given process is not being optimized to perfection using a variety of custom made miniature solutions. Most importantly in bioprocess development scalability must always stay in focus. As soon as during the experimental design for lab scale we constrain the design-space of process variables to the technical feasible space in target production scale. This ensures the transferability of the developed process to the final production unit and effectively helps to minimize scaling issues. </p>
    </div>

    <div class="short-intro-about-scientific" id="011">
      <img src="img/scaleup.png" alt="" class="technology-img-left-3">
      <h2 class="short-intro-h2-phytotechnicals" id="">Cells – Producers in Focus</h2>
      <p class="short-intro-p">Scientific literature illustrates a long history of optimizing bioprocesses by merely optimizing input/output rates of the bioreactor. In contrast to a long history of regarding the "bioreactor" as productive entity, state of the art technologies allow us today to take a closer look at a cellular level. This change of focus appears self-evident - the actual producer of a given product, are the cells/organism inside the bioreactor. This change of focus is correlated with an increased analytical effort, but promises significant increases in productivity.</p>
    </div>





  </div>



  <div class="footer">
      <div class="footer-separator"></div>
      <div class="footer-left">
        <p class="contact">Contact:</p>
        <p class="buro">office +43 324324856 </p>
        <a class="office" href="mailto:office@evologic.at?subject=feedback" target="_blank">office@evologic.at</a><br>
        <a class="www" href="http://www.evologic.at" target="_blank"> www.evologic.at</a>
      </div>
      <div class="footer-right">
        <ul class="ul-footer">
          <li class="home"><a class="home" href="http://www.evologic.at" target="_blank">Home</a></li>
          <li><a class="agb" href="agb.php" target="_blank">AGB</a></li>
          <li class="impli"><a class="imp" href="impressum.php" target="_blank">Impressum</a></li>
        </ul>
      </div>
  </div>

  <script>

      var elements = document.getElementsByClassName("child");
      for(var i = 0; i < elements.length; i++)
      {
          elements[i].onclick = function(){

              // remove class from sibling

              var el = elements[0];
              while(el)
              {
                  if(el.tagName === "DIV"){
                      //remove class
                      el.classList.remove("bak");

                  }
                  // pass to the new sibling
                  el = el.nextSibling;
              }

            this.classList.add("bak");
          };
      }

  </script>
<script type="text/javascript">jssor_1_slider_init();</script>
<script type="text/javascript" src="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.min.js"></script>
<script src="js/jquery-2.1.4.js"></script>
<script src="js/jquery.mobile.custom.min.js"></script>
<script src="js/main.js"></script> <!-- Resource jQuery -->
<script src="js/js/smooth.js"type="text/javascript"></script>
<script src="js/top.js" type="text/javascript"></script>
</body>
</html>

<?php
session_start();
$loggedIn = false;
if (isset($_SESSION['uid'])) {
    $loggedIn = true;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Evologic - Grow. Different.</title>
  <link rel="icon" href="img/favicon.jpg" type="image/gif" sizes="16x16">
  <link rel="stylesheet" type="text/css" href="css/reset.css">
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <link rel="stylesheet" type="text/css" href="css/slider.css">
    <!-- <link rel="stylesheet" type="text/css" href="css/change.css"> -->
  <script src="js/jssor.slider-27.1.0.min.js" type="text/javascript"></script>
  <script src="js/slider.js" type="text/javascript"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <!-- <script src="js/scroll.js" type="text/javascript"></script>
  <script src="js/ss.js" type="text/javascript"></script>
  <script src="js/kk.js"type="text/javascript"></script> -->
  <script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
  <script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.css"/>
  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick-theme.css"/>
  <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300italic,regular,italic,700,700italic&subset=latin-ext,greek-ext,cyrillic-ext,greek,vietnamese,latin,cyrillic" rel="stylesheet" type="text/css" />
  <script src='https://www.google.com/recaptcha/api.js'></script>
  <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Cabin:400,700" rel="stylesheet">
</head>
<body>
  <a href="javascript:" id="return-to-top"><i class="icon-chevron-up"></i></a>
  <div class="menu-wrapper-home">
   <div class="menu">
      <a href="http://www.evologic-technologies.com/" id="logo-link"><img src="img/logo1.png" alt="Evologic Techonologies" /></a>
      <div class="menu-entries top" id="menu-entries" >
          <!-- <div id="menu-entry-1" class="tooltip hov"> -->
              <a href="technology.php" class="main-nav">Technology</a>
          <!-- </div> -->
          <!-- <div  id="menu-entry-2" class="tooltip2 hov"> -->
              <a href="products.php" class="main-nav">Product</a>
          <!-- </div> -->
          <!-- <div  id="menu-entry-3" class="tooltip3 hov"> -->
              <a href="service.php" class="main-nav">Service</a>
          <!-- </div> -->
          <!-- <div  id="menu-entry-3" class="tooltip3 hov"> -->
              <a href="about.php" class="main-nav">About Us</a>
          <!-- </div> -->
          <!-- <div id="menu-entry-3" class="tooltip3 hov"> -->
              <a href="investor.php" class="main-nav">Investor Relations</a>
          <!-- </div> -->
      </div>
      <div class="clear"></div>
    </div>

    <section class="top-nav">
      <div>
        <a href="http://www.evologic-technologies.com/" id="logo-link"><img src="img/logo1.png" alt="Evologic Techonologies" /></a>
      </div>

      <input id="menu-toggle" type="checkbox" />
      <label class='menu-button-container' for="menu-toggle">
      <div class='menu-button'></div>
    </label>
      <ul class="menu-mobile">
        <li><a href="technology.php" class="mobile-nav">Technology</a></li>
        <li><a href="products.php" class="mobile-nav">Product</a></li>
        <li><a href="service.php" class="mobile-nav">Service</a></li>
        <li><a href="about.php" class="mobile-nav">About Us</a></li>
        <li><a href="investor.php" class="mobile-nav">Investor Relations</a></li>
      </ul>
  </section>



  </div>
  <div class="header-separator" id="header-separator-home"></div>
  <div id="jssor_1" style="position:relative;top:130px;left:0px;width:1300px;height:360px;overflow:hidden;visibility:hidden;">
      <!-- Loading Screen -->
      <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
          <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="img/spin.svg" />
      </div>
      <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:1300px;height:340px;overflow:hidden;">
          <!-- <div data-p="225.00">
              <img data-u="image" src="img/001.jpg" class="banner-img" />
              <img data-u="image" src="img/transition.png" class="banner-transition" />
              <div class="banner-text-1">
                Grow. Different.
                <div class="banner-text-small-1">Into a Sustainable Future</div>

              </div>
          </div>
          <div data-p="225.00">
              <img data-u="image" src="img/002.jpg" class="banner-img" />
              <img data-u="image" src="img/transition.png" class="banner-transition" />
              <div class="banner-text-2">
                Grow. Different.
                <div class="banner-text-small-2">Bio Tech Solutions for Resource Efficiency</div>
              </div>
          </div> -->
          <!-- <div data-p="225.00">
              <img data-u="image" src="img/landing-banner1.jpg" class="banner-img" />
              <img data-u="image" src="img/transition.png" class="banner-transition" />
              <div class="banner-text-3">
                Grow. Different.
                <div class="banner-text-small-3">Shaping the Future with Biologicals</div>

              </div>
          </div> -->
      </div>
      <!-- Bullet Navigator -->
      <!-- <div data-u="navigator" class="jssorb032" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
          <div data-u="prototype" class="i" style="width:16px;height:16px;">
              <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                  <circle class="b" cx="8000" cy="8000" r="5800"></circle>
              </svg>
          </div>
      </div> -->
      <!-- Arrow Navigator -->
      <!-- <div data-u="arrowleft" class="jssora051" style="width:65px;height:65px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
          <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
              <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
          </svg>
      </div>
      <div data-u="arrowright" class="jssora051" style="width:65px;height:65px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
          <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
              <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
          </svg>
      </div> -->
  </div>

  <div data-p="225.00">
      <img data-u="image" src="img/landing-banner1.jpg" class="banner-img-main" />
      <img data-u="image" src="img/transition.png" class="banner-transition-main" />
      <div class="banner-text-3-main">
        Grow. Different.
        <div class="banner-text-small-3-main">Shaping the Future with Biologicals</div>

      </div>
  </div>
  <!-- <div class="short-intro">
    <p class="short-intro-p" id="short-intro-p">The Spin-Off Evologic Technologies focuses on bioprocess development, optimization and upscaling.<br>Translating advanced process technologies from red biotech for green and white biotech products gives us a competitive edge.<br>A strong academic and industrial network supports and facilitates an unprecedentedly efficient transformation of product ideas <br> from the lab to viable production processes. </p>
  </div> -->
  <div class="pages-wrapper middle">
    <div class="logos1 ll">
      <p id="randd">Mission</p>
      <img class="logos-img" src="img/icons/mission1.png" alt="R&D" style="width:50px;display: inline-block;">

      <p class="unlock-p">Unlocking the potential of phytochemicals and biologicals.</p>
      <!-- <div class="overlay1"></div> -->
      <!-- <div class="button"><a href="r&d.php"> Read More! </a></div> -->
    </div>
    <div class="mainlogos">

      <div class="logos2 ll">
        <img class="logos-img" src="img/icons/technology1.png" alt="Products" style="width:100px;display: block;  margin: 0 auto;  margin-top: 20px;">
        <p class="application">Technology</p>
        <p class="logos2-p">Our patented platform technology allows to grow hairy roots in industrial scale bioreactors.</br></br>
        Despite the numerous lucrative phytochemicals, to this date the production of hairy roots has not exceeded 70 L. Our novel production technology allows scaling hairy root production to several m3. Our bioreactor design allows, for the first time the production of hairy roots at industrial scale.</p>
        <!-- <div class="overlay2"></div> -->
        <!-- <div class="button"><a href="products.php"> Read More! </a></div> -->
      </div>
      <div class="logos3 ll">
        <img class="logos-img" src="img/icons/product1.png" alt="About Us" style="width:100px;height: 101px; display: block; margin: 0 auto; margin-top: 20px;">
        <p class="company">Product</p>
        <p class="logos3-p">Yield increasing fungi, applied as biological seed coating, leverage sustainability in agriculture</br></br>
          Besides secondary metabolites, hairy roots also allow the production of symbiotic fungi.  These symbiotic fungi are called Mycorrhiza and have been shown to decrease the need for chemical fertilizers and even water. To date, mycorrhiza products are too expensive, and vary in quality. Our novel production process for hairy roots allows to produce mycorrhiza at economically feasible costs for the first time..</p>

        <!-- <div class="overlay3"></div> -->
        <!-- <div class="button"><a href="about.php"> Read More! </a></div> -->
      </div>

      <div class="logos4 ll">
        <img class="logos-img" src="img/icons/service1.png" alt="About Us" style=" width: 120px; height: 101px;  display: block;  margin: 0 auto;  margin-top: 20px;">
        <p class="company">Service</p>
        <p class="logos4-p">Cooperation is key to innovation the basis to find the best solutions.</br></br>
        At the intersection of academia  and industry  we can effectively develop high tech solutions cost and time efficiently. Find out more about our development capabilities and rationales, which allow highly efficient bioprocess engineering and development.</p>
        <!-- <div class="overlay3"></div> -->
        <!-- <div class="button"><a href="about.php"> Read More! </a></div> -->
      </div>
    </div>
  </div>
  <!--partner logo slider -->

  <!-- <div class="logo-container"> -->
  <div class="partner-container">
    <div class="partners">
      <h1 class="partner-h1">Partners</h1>
       <section class="customer-logos slider">
          <div class="slide"><img src="img/partner_logos/1.png" style="width:190px;margin-bottom: 50px;"></div>
          <div class="slide"><img src="img/partner_logos/6.png" style="width:190px;margin-bottom: 50px;"></div>
          <div class="slide"><img src="img/partner_logos/5.png" style="width:190px;margin-bottom: 50px;"></div>
       </section>
    </div>

    <div class="partners2">
      <h1 class="partner-h1">Supporters</h1>
       <section class="customer-logos slider">
          <div class="slide"><img src="img/partner_logos/4.png" style="width:190px;margin-bottom: 50px;"></div>
          <div class="slide"><img src="img/partner_logos/2.jpg" style="width:190px;margin-bottom: 30px;"></div>
          <div class="slide"><img src="img/partner_logos/8.jpg" style="width:190px;margin-bottom: 50px;"></div>
       </section>
    </div>
  </div>
  <!-- partner logo slider end -->
  <?php



  require_once'security.php';

  $errors = isset($_SESSION['errors']) ?  $_SESSION['errors'] : [];
  $fields = isset($_SESSION['fields']) ?  $_SESSION['fields'] : [];

  ?>
  <div class="contact-form">
    <div class="join-us">
      <h1>Want to join/participate? - reach out to us</h1>
      <p class="join-us-p">We are always looking for highly self-motivated team players as well as additional investments to move forward faster. As a team we enjoy scientific reasoning and challenging each other to identify the most practical solution with the highest probability to enable progress. If interested in the topic, wanting to contribute financially as well as practically please use the contact form. We are looking forward to hearing from you!</p>
    </div>
    <h1>Contact Us</h1>
    <div class="container">
      <form id="contact-form" method="post" action="check.php">
        <label for="fname">Full Name</label>
        <input type="text" name="title" class="hfield">
        <input type="text" id="form_fname" name="name" placeholder="Full Name" >
        <span style="color:red;" class="error_form" id="fname_error_message"></span>
        <input type="text" id="form_email" name="email" placeholder="Email Address">
        <span style="color:red;" class="error_form" id="email_error_message"></span>
        <textarea id="form_message" name="message" placeholder="Your Message" style="height:150px" ></textarea>
        <span style="color:red;" class="error_form" id="message_error_message"></span>
        <div style="margin-bottom: 10px; margin-top:5px;"class="g-recaptcha" data-sitekey="6LcfclcUAAAAAGNylZI8zih_qfHfF7yYg7D8F0CQ"></div>
        <input type="submit" onclick="scrollToElement('#contact-form');" value="Submit">
      </form>
      <?php
        unset($_SESSION['errors']);
        unset($_SESSION['fields']);
      ?>
    </div>
    <div class="frame">
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d9474142.548590409!2d8.562849457341615!3d48.199281260462456!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x476d0785a5a1eec5%3A0xc67c7257c0a7cc9f!2sGumpendorfer+Str.+19%2C+1060+Wien!5e0!3m2!1sen!2sat!4v1527257347518" width="450" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
  </div>
  <div class="footer">
      <div class="footer-separator"></div>
      <div class="footer-left">
        <p class="contact">Contact:</p>
        <p class="buro">office +43 324324856 </p>
        <a class="office" href="mailto:office@evologic.at?subject=feedback" target="_blank">office@evologic.at</a><br>
        <a class="www" href="http://www.evologic.at" target="_blank"> www.evologic.at</a>
      </div>
      <div class="footer-right">
        <ul class="ul-footer">
          <li class="home"><a class="home" href="http://www.evologic.at" target="_blank">Home</a></li>
          <li><a class="agb" href="agb.php" target="_blank">AGB</a></li>
          <li class="impli"><a class="imp" href="impressum.php" target="_blank">Impressum</a></li>
        </ul>
      </div>
  </div>

  <script>



    $(function(){
      $("#fname_error_message").hide();
      $("#email_error_message").hide();
      $("#message_error_message").hide();
      var error_fname = false;
      var error_message = false;
      var error_email = false;

      $("#form_fname").focusout(function(){
        check_fname();
      });
      $("#form_email").focusout(function() {
        check_email();
      });
      $("#form_message").focusout(function() {
        check_message();
      });

      function check_fname() {
      var pattern = /^[a-zA-Z 0-9\n\\-\\s]+$/;
      var fname = $("#form_fname").val();
      if (pattern.test(fname) && fname !== '') {
      $("#fname_error_message").hide();
      $("#form_fname").css("border-bottom","2px solid #34F458");
      } else {
      $("#fname_error_message").html("Please type your name");
      $("#fname_error_message").show();
      $("#form_fname").css("border-bottom","2px solid #F90A0A");
      error_fname = true;
      }
      }

      function check_message() {
      var pattern = /^[a-zA-Z 0-9\n\\-\\s]+$/;
      var message = $("#form_message").val()
      if (pattern.test(message) && message !== '') {
      $("#message_error_message").hide();
      $("#form_message").css("border-bottom","2px solid #34F458");
      } else {
      $("#message_error_message").html("Message should only contain characters,numbers and space");
      $("#message_error_message").show();
      $("#form_message").css("border-bottom","2px solid #F90A0A");
      error_message = true;
      }
      }

      function check_email() {
      var pattern = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
      var email = $("#form_email").val();
      if(pattern.test(email) && email !== '') {
      $("#email_error_message").hide();
      $("#form_email").css("border-bottom","2px solid #34F458");
      } else {
      $("#email_error_message").html("Invalid Email");
      $("#email_error_message").show();
      $("#form_email").css("border-bottom","2px solid #F90A0A");
      error_email = true;
      }
      }

      $("#contact-form").submit(function() {
      error_fname = false;
      error_message = false;
      error_email = false;

      check_fname();
      check_message();
      check_email();
      if (error_fname === true) {
      alert("Please Fill the form Correctly");
      return false;
    }
      if (error_message === true) {
      alert("Please Fill the form Correctly");
      return false;
    }
      if (error_email === true) {
      alert("Please Fill the form Correctly");
      return false;
    }
      if(grecaptcha && grecaptcha.getResponse().length > 0)
        {
          alert("Thank you for your message, we will contact you soon!");
          return true;
        }
        else
        {
            alert('Oops, you have to check the recaptcha !');
            return false;
        }
      });
    });


      var elements = document.getElementsByClassName("child");
      for(var i = 0; i < elements.length; i++)
      {
          elements[i].onclick = function(){

              // remove class from sibling

              var el = elements[0];
              while(el)
              {
                  if(el.tagName === "DIV"){
                      //remove class
                      el.classList.remove("bak");

                  }
                  // pass to the new sibling
                  el = el.nextSibling;
              }

            this.classList.add("bak");
          };
      }

  </script>
<script type="text/javascript">jssor_1_slider_init();</script>
<script type="text/javascript" src="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.min.js"></script>
<script src="js/top.js" type="text/javascript"></script>
</body>
</html>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Evologic - Grow. Different.</title>
  <link rel="icon" href="img/favicon.jpg" type="image/gif" sizes="16x16">
  <link rel="stylesheet" type="text/css" href="css/reset.css">
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <link rel="stylesheet" type="text/css" href="css/slider.css">
  <script src="js/jssor.slider-27.1.0.min.js" type="text/javascript"></script>
  <script src="js/slider.js" type="text/javascript"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="js/scroll.js" type="text/javascript"></script>
  <script src="js/ss.js" type="text/javascript"></script>
  <script src="js/kk.js"type="text/javascript"></script>
  <script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.css"/>
  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick-theme.css"/>
  <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300italic,regular,italic,700,700italic&subset=latin-ext,greek-ext,cyrillic-ext,greek,vietnamese,latin,cyrillic" rel="stylesheet" type="text/css" />
  <link href='https://fonts.googleapis.com/css?family=Playfair+Display:700,900|Fira+Sans:400,400italic' rel='stylesheet' type='text/css'>
  <link href="https://fonts.googleapis.com/css?family=Cabin:400,700" rel="stylesheet">
  <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
	<link rel="stylesheet" href="css/timeline.css"> <!-- Resource style -->
	<script src="js/modernizr.js"></script> <!-- Modernizr -->


</head>
<body>
  <a href="javascript:" id="return-to-top"><i class="icon-chevron-up"></i></a>
  <div class="menu-wrapper-home">
   <div class="menu">
      <a href="http://www.evologic-technologies.com/" id="logo-link"><img src="img/logo1.png" alt="Evologic Techonologies" /></a>
      <div class="menu-entries top" id="menu-entries" >
          <!-- <div id="menu-entry-1" class="tooltip hov"> -->
              <a href="technology.php" class="main-nav">Technology</a>
          <!-- </div> -->
          <!-- <div  id="menu-entry-2" class="tooltip2 hov"> -->
              <a href="products.php" class="main-nav selected-nav">Product</a>
          <!-- </div> -->
          <!-- <div  id="menu-entry-3" class="tooltip3 hov"> -->
              <a href="service.php" class="main-nav">Service</a>
          <!-- </div> -->
          <!-- <div  id="menu-entry-3" class="tooltip3 hov"> -->
              <a href="about.php" class="main-nav">About Us</a>
          <!-- </div> -->
          <!-- <div id="menu-entry-3" class="tooltip3 hov"> -->
              <a href="investor.php" class="main-nav">Investor Relations</a>
          <!-- </div> -->
      </div>
      <div class="clear"></div>
    </div>

    <section class="top-nav">
      <div>
        <a href="http://www.evologic-technologies.com/" id="logo-link"><img src="img/logo1.png" alt="Evologic Techonologies" /></a>
      </div>

      <input id="menu-toggle" type="checkbox" />
      <label class='menu-button-container' for="menu-toggle">
      <div class='menu-button'></div>
    </label>
      <ul class="menu-mobile">
        <li><a href="technology.php" class="mobile-nav">Technology</a></li>
        <li><a href="products.php" class="mobile-nav">Product</a></li>
        <li><a href="service.php" class="mobile-nav">Service</a></li>
        <li><a href="about.php" class="mobile-nav">About Us</a></li>
        <li><a href="investor.php" class="mobile-nav">Investor Relations</a></li>
      </ul>
  </section>



  </div>
  <div class="header-separator" id="header-separator-products"></div>
  <div data-p="225.00">
      <img data-u="image" src="img/product-banner1.png" class="banner-img-product" />
      <img data-u="image" src="img/transition.png" class="banner-transition-product" />
      <div class="banner-text-3-product">

        <div class="banner-text-small-3-technology">Our novel technology allows large scale <br>AMF production at a competitive price</div>
      </div>
  </div>
  <div class="sidenav">
    <img src="img/icons/product1.png" alt="" class="technology-img-left">

    <ul>
      <li class="actives ">
        <a href="#11" class="scroll" >AMF Advantages</a>
      </li>
      <li class="acc ">
        <a href="#p22" class="scroll right-side">Soil Volume</a>
      </li>
      <li class="acc ">
        <a href="#33" class="scroll right-side" >Nutrient Uptake</a>
      </li>
      <li class="acc ">
        <a href="#44" class="scroll right-side" >Water Efficiency</a>
      </li>
      <!-- kk -->

      <li class="acc ">
        <a href="#55" class="scroll" >AMF Production</a>
      </li>
      <li class="acc ">
        <a href="#66" class="scroll right-side" >In Vivo</a>
      </li>
      <li class="acc ">
        <a href="#77" class="scroll right-side" >In Vitro</a>
      </li>
      <li class="acc ">
        <a href="#88" class="scroll right-side" >Pricing</a>
      </li>

      <!-- lll -->
      <li class="acc ">
        <a href="#99" class="scroll" >Novel Production technology</a>
      </li>
      <li class="acc ">
        <a href="#10" class="scroll right-side" >Scalability</a>
      </li>
      <li class="acc ">
        <a href="#1010" class="scroll right-side" >Product Quality</a>
      </li>

      <!-- <li class="actives ">
        <a href="#what" class="scroll" >n vivo production Icon greenhouse</a>
      </li>
      <li class="acc ">
        <a href="#mainamf" class="scroll" >in vitro production Icon Shake flask</a>
      </li>
      <li class="acc ">
        <a href="#mainamf" class="scroll" >in vitro production Icon Shake flask</a>
      </li>
      <li class="acc ">
        <a href="#mainamf" class="scroll" >usage of AMF</a>
      </li>
      <li class="acc ">
        <a href="#mainamf" class="scroll" >Our novel technology</a>
      </li> -->

    </ul>
  </div>
  <div class="content-product1">



    <div id="11"class="short-intro-about">
      <h2 class="short-intro-h2-products amf-product" >AMF, a natural symbiotic fungus increases fertilizer efficiency</h2>
      <p class="short-intro-p">AMF is the acronym for Arbuscular Mycorrhizal Fungi. Over 90% of terrestrial plants naturally live in symbiosis with mycorrhiza. While the fungus gets fed with biological building blocks (as fatty acids and sugars), the fungi supplies nutrients like salts to the plant. In symbiosis with the host plant, the accessible soil volume is massively increased and nutrients are actively transported into the host plant. This symbiosis is nature´s solution for scarce nutrients. Our product facilitates the utilization of this natural approach of yield increase in conventional agriculture.</p>
      <img class="fertilizer-img" id="fertilizer-img" src="img/fertilizer.png" alt="" style="width:70%;">
      <h2 class="short-intro-h2-products" id="nutrient-delivery-tech">Advantages of AMF use in agriculture </h2><img data-u="image" src="img/transition.png" class="banner-transition-tech2" style="width:70%;">

    </div>


    <div class="short-intro-about-scientific" id="p22">
      <h2 class="massive" ><br>Massively increased accessible soil volume</h2>
      <p class="short-intro-p-tech">Plants get their nutrients from the soil the root can access. Consequently, a high nutrient concertation in the soil leads to a higher crop yield than a low nutrient concentration. However, this correlation is limited by the capacity of the soil to retain nutrients. Rain water takes the nutrients into a lower soil level. There, the nutrients are out of reach for the plant roots. Plant roots can actively transport nutrients from the soil in the range of only 1 mm around the circumference of the root. The structure of the mycelium of mycorrhiza is far more delicate than the macroscopic plant roots and can reach much more of the given soil volume. Plants that form a symbiosis with mycorrhiza increase their accessible soil volume 4-15-fold (Jansa et al 2003). </p>
    </div>


    <div class="short-intro-about-scientific"  id="33">
      <h2 class="short-intro-h2-products-4">+150% increase in nutrient uptake efficiency</h2>
      <p class="short-intro-p">The active nutrient uptake via AMF improves the plants nutrient uptake (+175-190%; Li-Ping et. al. 2009) and thus naturally enhances the overall crop yield. Nutrient losses by washout of e.g. Phosphorus (33%) and Nitrogen (36%) are approximately cut in half, to 15-16%.  Consequently, fertilizer application can be reduced by 15% and crops benefit from enhanced growth and increased biomass. </p>
    </div>

    <div class="short-intro-about-scientific"  id="44">
      <h2 class="short-intro-h2-products" id="whereis">Increased water efficiency</h2>
      <p class="short-intro-p">Furthermore, the symbiosis with AMF increases the crop’s resistance to disease and its tolerance to draught. Independent studies have demonstrated that AMF treated crops show an approximately 20% higher crop yield than untreated crops (Subramanian et al, 2005; Torres-Barragàn 1996). This yield increase owned to AMF is even more pronounced under increasingly arid conditions. Water is bound and consequently retained within the soil horizon accessible to the plant via Glomaline, (Biopolymer) produced by AMF.</p>
    </div>

    <div class="short-intro-about-scientific-last-products succeed"  id="55" >
      <h2 class="short-intro-h2-products" id="nutrient-delivery-tech">State of the art production methods are not scalable</h2><img data-u="image" src="img/transition.png" class="banner-transition-tech2-3" style="width:70%;">
      <p class="short-intro-p"><br><br><br>Mycorrhiza cannot reproduce without a host due to its inability to produce the necessary building blocks on its own. This means mycorrhiza can only survive and reproduce in symbiosis with a plant. The fungi start their lifecycle as spores, which contain a limited supply of nutrients. The fungus must engage in symbiosis with a plant to gain access to an exchange relation. Without a host, the fungus is prone to die of starvation. Only if the fungus is successful and “finds” a plant, trace elements and nutrients can be exchanged against sugars and other fatty acids.</p>
    </div>

    <div class="products-icons">
      <div class="product-66" id="66">
        <img src="img/icons/vitro3.png" alt="" class="img-product-icons">
        <h2 class="products-icons-vito">In vivo production</h2>
        <p class="short-intro-p">The conventional approach for Mycorrhiza production is "in vivo". Whole plants are inoculated (infected) with mycorrhiza and grown in green houses in a growth substrate made of e.g. perlite/sand or other porous carrier materials. In symbiosis with the plant, both parties grow. The fungi multiplies in the growth substrate. At the end of the growth period everything is let to dry, the green parts of the plant are removed and the substrate including plant roots and mycorrhiza is homogenized. This production approach is very close to the natural way of mycorrhiza reproduction, but on the other hand, the environmental factors can only be controlled to a certain extend in a green house.</p>
      </div>
      <div class="product-77" id="77" >
        <img src="img/icons/vitro1.png" alt="" class="img-product-icons2">
        <h2 class="products-icons-vito">in vitro production</h2>
        <p class="short-intro-p">A more advanced approach is based on growing transformed roots in glassware. A bacterium, Agrobacillus is used to naturally transform the plant root. This transformed root can grow without leaves or green parts. Despite the significant metabolic changes induced by Agrobacillus, these transformed roots are not genetically modified, since Agrobacillus tranformations naturally occur in nature. These transformed roots lack the green shoot. The necessary nutrients and are therefore supplied via the growth media. These transformed roots are called "hairy roots" and are commonly grown on solid or in liquid growth media. This "in-vitro" production approach allows the production of Mycorrhiza all year round, independent of climatic conditions and vegetative cycles of the host plant. The downside of this process is the requirement for substantial amounts of manual labor. In addition, the amount of manual labor grows in proportion to the production volume. </p>
      </div>

    </div>



    <div class="short-intro-about-scientific-last-products succeed" id="88" >
      <h2 class="short-intro-h2-products" id="succeed">Current pricing is hardly attractive</h2>
      <p class="short-intro-p">Unfortunately, currently available AMF products make it difficult to gain an economic benefit from the usage of AMF. Various mycorrhiza products are sold worldwide. The price comparison is extremely complex in general, not only for the end user. Besides containing different AMF species, the concentration of the active ingredient (the spores) is hardly ever specified. This is of high relevance since the number of spores directly drives the effect but also the cost of goods during production. Given the high effort necessary for mycorrhiza production correlated with conventional in-vitro production approaches, the cost of goods for the production of mycorrhiza make an economically feasible pricing hardly possible.</p>
    </div>
    <div class="short-intro-about-scientific-last-products succeed"id="99" >
      <h2 class="short-intro-h2-products" id="nutrient-delivery-tech">AMF production at a disruptive price</h2><img data-u="image" src="img/transition.png" class="banner-transition-tech2-products"><br><br><br>
      <p class="short-intro-p" id="evo-pro-99">Evologic has developed a novel in vitro production process based on a bioreactor instead of shake flasks. This patented production process allows to tap into the economy of scale and can be used for various AMF types.</p>
    </div>

    <div class="short-intro-about-scientific-last-products succeed great-scale" id="10" >
      <h2 class="short-intro-h2-products" id="succeed">Great Scalability</h2>
      <p class="short-intro-p" id="great-scal">This approach allows to use the economy of scale since the amount of manual labor stays constant even for a production run at > 50m3. Hereby, we are transferring bioprocess control and -optimization routines originating from pharma industry to achieve high product quality. </p>
    </div>
    <div class="short-intro-about-scientific-last-products succeed"id="1010" >
      <h2 class="short-intro-h2-products hqaulity" id="succeed">High Product Quality</h2>
      <p class="short-intro-p">Moreover, we have been investing substantial effort in establishing novel methods of analysis in order to ensure product quality and quantity. This allows the application of the quality of design principles starting at lab-scale to ensure a high performance product at an unrivaled low selling price.</p>
</p>
    </div>


  </div>



  <div class="footer">
      <div class="footer-separator"></div>
      <div class="footer-left">
        <p class="contact">Contact:</p>
        <p class="buro">office +43 324324856 </p>
        <a class="office" href="mailto:office@evologic.at?subject=feedback" target="_blank">office@evologic.at</a><br>
        <a class="www" href="http://www.evologic.at" target="_blank"> www.evologic.at</a>
      </div>
      <div class="footer-right">
        <ul class="ul-footer">
          <li class="home"><a class="home" href="http://www.evologic.at" target="_blank">Home</a></li>
          <li><a class="agb" href="agb.php" target="_blank">AGB</a></li>
          <li class="impli"><a class="imp" href="impressum.php" target="_blank">Impressum</a></li>
        </ul>
      </div>
  </div>

  <script>

      var elements = document.getElementsByClassName("child");
      for(var i = 0; i < elements.length; i++)
      {
          elements[i].onclick = function(){

              // remove class from sibling

              var el = elements[0];
              while(el)
              {
                  if(el.tagName === "DIV"){
                      //remove class
                      el.classList.remove("bak");

                  }
                  // pass to the new sibling
                  el = el.nextSibling;
              }

            this.classList.add("bak");
          };
      }

  </script>
<script type="text/javascript">jssor_1_slider_init();</script>
<script type="text/javascript" src="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.min.js"></script>
<script src="js/jquery-2.1.4.js"></script>
<script src="js/jquery.mobile.custom.min.js"></script>
<script src="js/main.js"></script> <!-- Resource jQuery -->
<script src="js/js/smooth.js"type="text/javascript"></script>
<script src="js/top.js" type="text/javascript"></script>
</body>
</html>
